''' Version 7.0 Planning model'''

# packages
import pickle
import timeit
import forward
import backward
import optimality
import sys, json
from utils import classes
from utils.paramvalidation import paramlimits
import logging

# Simulation time
start = timeit.default_timer()


logging.warning(sys.argv[0])
sys.stdout.flush()
params=json.loads(sys.argv[1])

logging.warning('Ejecutando python en el modelo ' + str(params['id']))
logging.warning('procesando ' + params['name'])
logging.warning('phase = ' + str(params['phase']))
logging.warning('statusId = ' + str(params['statusId']))
logging.warning('templateId = ' + str(params['templateId']))
logging.warning('max_iter = ' + str(params['max_iter']))
logging.warning('stages ' + str(params['stages']))
logging.warning('seriesBack = ' + str(params['seriesBack']))
logging.warning('seriesForw = ' + str(params['seriesForw']))
logging.warning('stochastic = ' + str(params['stochastic']))
logging.warning('variance = ' + str(params['variance']))
logging.warning('sensDem = ' + str(params['sensDem']))
logging.warning('speed_out = ' + str(params['speed_out']))
logging.warning('speed_in = ' + str(params['speed_in']))
logging.warning('eps_area = ' + str(params['eps_area']))
logging.warning('eps_all = ' + str(params['eps_all']))
logging.warning('eps_risk = ' + str(params['eps_risk']))
logging.warning('commit = ' + str(params['commit']))
logging.warning('lag_max = ' + str(params['lag_max']))
logging.warning('testing_t = ' + str(params['testing_t']))
logging.warning('d_correl = ' + str(params['d_correl']))
logging.warning('seasonality = ' + str(params['seasonality']))
start = timeit.default_timer()


#==============================================================================
# REDIS
import redis

r = redis.StrictRedis(host='localhost', port=6379, db=0)
logging.warning('room/' + str(params['id']))


#== Formato del mensaje
data = {}
data['id'] =  str(params['id'])
data['state'] = 'Inicializado'
data['message'] = 'Los datos han sido cargados'
data['phase'] = 3
data['percent'] = 0
json_data = json.dumps(data)
r.publish('room/' +str(params['id']), json_data)

#==============================================================================



#==============================================================================
# read options SI or NO
read_data = True
if read_data is True:

    #Reading system information
    import readData
    print('Reading data...')
    readData.data('datasystem/2areasprueba.xlsx')
    # delete temporal files

    data['state'] = 'readExcelInfo'
    data['message'] = 'Cargando datos'
    data['percent'] = 10
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)


#==============================================================================
# Load the dictionary back from the pickle file
dict_data = pickle.load(open("savedata/data_save.p", "rb"))

# No of maximum number of stages
noStages = len(dict_data['horizon'])

#==============================================================================
# Parameters simulation
max_iter = 1 # Maximun number of iterations
extra_stages = 1 # Boundary effects
stages = 5 + extra_stages # planning horizon: (months + extra months)
seriesBack = 2 # series used in the backward phase
seriesForw = 3 # series used in the forward phase
# deterministic / stochastic analysis
stochastic = True
if seriesBack == 1: stochastic = False # Deterministic

# Parameters analysis
sensDem = 1.0 # Demand factor
eps_area = 0.5 # Short-term - significance level area
eps_all = 0.5 # Short-term - significance level for the whole system
eps_risk = 0.05 # long-term risk
commit = 0.0 # risk-measure comminment

# validating input parameters
paramlimits(stages, seriesBack, seriesForw, noStages)

#==============================================================================
# export options SI or NO
results = True
if results is True:
    from utils.saveresults import printresults

    dpch = True # dispatch curves
    mcost = False # marginal cost
    curves = [dpch, mcost]

#==============================================================================
# calculation options SI or NO
parameters_calculation = True
if parameters_calculation is True:

    data['state'] = 'parametersCalculation'
    data['message'] = 'Calculando parametros'
    data['percent'] = 10
    data['phase'] = 3
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)

    print('Parameters calculation ...')

    # Creating fixed input files '''
    from utils.input_data import inputdata
    inputdata(dict_data, sensDem)

    from utils.input_hydro import inputhydro
    inputhydro(dict_data)

    from utils.input_wind import inputwindSet, inputInflowWind
    inputwindSet(dict_data, stages, 1, 0)
    inputInflowWind(dict_data, stages, eps_area, eps_all)

    from utils.input_others import inputbatteries, inputlines
    inputbatteries(dict_data, stages)
    inputlines(dict_data, stages)

#==============================================================================
# opf restrictions
param_opf = True
if param_opf is True:

    print('Parameters opf ...')
    from utils.opf_data import ybus
    ybus(dict_data, stages)

#==============================================================================
# Including wind power plants with model 2
wind_model2 = False
if wind_model2 is True:

    data['state'] = 'readWindData'
    data['message'] = 'Cargando datos de viento'
    data['percent'] = 10
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)


    print('Model of wind power plants with losses ...')
    from utils.readWind import historical10m, format10m

    historical10m()
    format10m()

    from utils.input_wind import inputwindSet, inputInflowWind, energyRealWind
    inputwindSet(dict_data, stages, 0, 1)
    factores = energyRealWind(dict_data, seriesBack, stages)

#==============================================================================
# Optimization section

# dictionaries
batteries = dict_data['batteries']
hydroPlants = dict_data['hydroPlants']
dict_batt = pickle.load(open("savedata/batt_save.p", "rb"))

# Iteration inf _ improve the states under evaluation
sol_vol = [[] for x in range(stages+1)] # Hydro iteration
sol_lvl = [[] for x in range(stages+1)] # Batteries iteration
sol_vol[0].append(dict_data['volData'][0])
sol_lvl[0].append([dict_data['battData'][0][x]*dict_batt["b_storage"][x][0][0] for x in range(len(batteries))])

iteration = 0 # Counter for number of iterations
confidence = 0 # stop criteria

# Save operational cost by iteration
operative_cost = [[], []]

###############################################################################
# define: policy and/or simulation
policy = True
simulation = True
parallel = False 
dataParamB = classes.param_B(stages, seriesBack, stochastic, eps_risk, commit, parallel, param_opf)
dataParamF = classes.param_F(stages, seriesForw, max_iter, results, param_opf)

###############################################################################

# loop iterations
if policy is True and simulation is True:




    while not iteration >= max_iter and not confidence >= 2:

        # save the FCF - backward steps
        fcf_backward = [[] for x in range(stages+1)]
        fcf_backward[stages]=[[[0]*len(hydroPlants), [0]*len(batteries),0]]

        # Backward_Risk6_par to parallel simulation
        fcf_backward = backward.data(dataParamB, fcf_backward, sol_vol, iteration, sol_lvl)

        data['state'] = 'loopIterations'
        data['message'] = 'Iterando backward ' + str(iteration)
        data['phase'] = 3
        data['percent'] = ((iteration + 1) / max_iter) * 50
        json_data = json.dumps(data)
        print(json_data)
        r.publish('room/' +str(params['id']), json_data)


        # Forward stage - Pyomo module
        (sol_vol, sol_lvl, sol_costs, sol_scn) = forward.data(confidence, fcf_backward,
        dataParamF, iteration, sol_vol, sol_lvl)

        data['state'] = 'loopIterations'
        data['message'] = 'Iterando Forward ' + str(iteration)
        data['phase'] = 3
        data['percent'] = ((iteration + 1) / max_iter) * 100
        json_data = json.dumps(data)
        print(json_data)
        r.publish('room/' +str(params['id']), json_data)

        # confidence
        confd, op_cost, op_inf = optimality.data(sol_costs, seriesForw)
        confidence += confd
        iteration += 1

        # Saver results
        operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)

        if iteration == max_iter or confidence == 2:
            datafcf = {"fcf_backward":fcf_backward, "sol_vol":sol_vol, "sol_lvl":sol_lvl, 'sol_scn':sol_scn}
            pickle.dump(datafcf, open("savedata/fcfdata.p", "wb"))

            # generate report
            if results is True:
                print('Writing results ...')
                # results files and reports
                printresults(seriesForw, (stages-extra_stages), sol_scn, curves)

elif policy is False and simulation is True:

    # stages to be simulate
    dict_fcf = pickle.load(open("savedata/fcfdata.p", "rb"))
    fcf_backward = dict_fcf['fcf_backward']

    sol_voll = dict_fcf['sol_vol']
    sol_lvl = dict_fcf['sol_lvl']
    iteration = 0
    confidence = 1

    # Forward stage - Pyomo module
    (sol_vol, sol_lvl, sol_costs, sol_scn) = forward.data(confidence, fcf_backward,
    dataParamF, iteration, sol_vol, sol_lvl)

    # Saver results
    confidence, op_cost, op_inf = optimality.data(sol_costs,seriesForw)
    operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)

    # generate report
    if results is True:
        print('Writing results ...')
        # results files and reports
        printresults(seriesForw, (stages-extra_stages), sol_scn, curves)

elif policy is False and simulation is False:

    if results is True:

        # recover results
        dict_fcf = pickle.load(open("savedata/fcfdata.p", "rb"))
        sol_scn = dict_fcf['sol_scn']

        print('Writing results ...')
        # results files and reports
        printresults(seriesForw, (stages-extra_stages), sol_scn, curves)

###############################################################################

# print execution time
end = timeit.default_timer()
print(end - start)

# Enviamos el evento para indicar que ha sido procesado
data = {}
data['id'] =  str(params['id'])
data['state'] = 'Finalizado'
data['phase'] = 3
data['message'] = 'El modelo ha sido procesado'
data['percent'] = 100
json_data = json.dumps(data)
r.publish('room/' +str(params['id']), json_data)

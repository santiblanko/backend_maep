
def pyomoformat(set1, set2, param):

    """
    Function that flattens the multidimensional dispaset input data into the pyomo format:
    a dictionnary with a tuple and the parameter value.
    The tuple contains the strings of the corresponding set values
    """

    param_new = {}; data = 0
    for k in set1:
        for z in set2:
            param_new[(k, z)] = param[data]
            data = data + 1

    return param_new

###############################################################################

def cutsback(stage,dict_data,sol_vol,iteration,sol_lvl,dict_batt):

    hydroPlants = dict_data['hydroPlants']
    batteries = dict_data['batteries']
    b_storage = dict_batt['b_storage']
    battData = dict_data['battData']
    volData = dict_data['volData']

    # calculate the volume states
    if stage == 1:
        # initial conditiona
        cuts_reservoirs = []
        for m in range(len(hydroPlants)): # Loop for all hydro plants
            vol_cut = [volData[0][m]]
            cuts_reservoirs.append(vol_cut)
    else:
        # empty reservoirs
        cuts_reservoirs = []
        for m in range(len(hydroPlants)): # Loop for all hydro plants
            vol_cut = [volData[1][m]]
            cuts_reservoirs.append(vol_cut)

    # calculate the storage cuts
    cuts_storage = []
    for m in range(len(batteries)): # Loop for all batteries
        lvl_cut = [b_storage[m][stage-1][1]*battData[1][m]]
        cuts_storage.append(lvl_cut)

    if iteration ==0:

        # Hydro plants
        cuts_iter_stage = []
        cuts_iter_stage.extend(cuts_reservoirs)
        # Batteries
        cuts_iter_stage_B = []
        cuts_iter_stage_B.extend(cuts_storage)

    else:

        # HYDRO - Results from last iteration
        cuts_iter_stage = [[] for x in range(len(hydroPlants))]
        # Loop for include vol_fin from the last iteration
        for c in range(len(hydroPlants)):
            cuts_iter_stage[c] = list(cuts_reservoirs[c])
        for c in range(len(hydroPlants)):
            for k in range(len(sol_vol[stage-1])):
                cuts_iter_stage[c].append(sol_vol[stage-1][k][c])
                # aux_vec = cuts_iter_stage[c]; print(aux_vec)


        cuts_iter_stage_B = [[] for x in range(len(batteries))]
        # Loop for include lvl_fin from the last iteration
        for c in range(len(batteries)):
            cuts_iter_stage_B[c] = list(cuts_storage[c])
        for c in range(len(batteries)):
            for k in range(len(sol_lvl[stage-1])):
                cuts_iter_stage_B[c].append(sol_lvl[stage-1][k][c])

    return cuts_iter_stage,cuts_iter_stage_B

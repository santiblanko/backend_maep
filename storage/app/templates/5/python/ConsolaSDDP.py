''' Version 7.8 Planning model'''
import logging

# packages
import timeit
from scripts import main_model
import sys, json
# Simulation time
start = timeit.default_timer()

#==============================================================================

# Parameters simulation
file = '2areasprueba'

max_iter = -1            # Maximun number of iterations
bnd_stages = 1          # Boundary stages
stages = 5 + bnd_stages # planning horizon: (months + bundary months)
seriesBack = 1          # series used in the backward phase
seriesForw = 1          # series used in the forward phase

# Parameters analysis
sensDem = 1.0   # Demand factor
eps_area = 0.5  # Short-term - significance level area
eps_all = 0.5   # Short-term - significance level for the whole system
eps_risk = 0.1  # long-term risk
commit = 0.0    # risk-measure comminment

# read data options
read_data = False
param_calculation = True 
param_opf = True         # OPF model
wind_model2 = False      # Second model of wind plants
flow_gates = True        # Security constraints   

# operation model options
policy = True
simulation = True
parallel = False 

# PRINT ORDER: 1. dispatch curves, 2. marginal cost
results = True
curves = [True, False]


	#==============================================================================
	# read data
	main_model.data(read_data, file)
	main_model.data_consistency(stages, seriesBack, seriesForw)
	main_model.parameters(param_calculation, sensDem, stages, eps_area, eps_all)
	main_model.grid(param_opf, stages)
	main_model.wmodel2(wind_model2, stages, seriesBack)
	main_model.optimization(stages, seriesBack, eps_risk, commit, parallel, param_opf, max_iter,
	                        results, policy, simulation, seriesForw, bnd_stages, curves, flow_gates)
	#==============================================================================

	# print execution time
	end = timeit.default_timer()
	print(end - start)

# HOMEWORK

# sedimentation curve
# unit commitment
# double circuits
# speed indices
# loses transmmision
# opf constraint
# distributed solar
# emissions


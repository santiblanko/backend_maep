''' Version 6.0 Planning model'''

# Simulation time
import timeit
import sys, json
import base64
import pprint
import logging


logging.warning(sys.argv[0])
sys.stdout.flush()
params=json.loads(sys.argv[1])

logging.warning('Ejecutando python en el modelo' + str(params['id']))
logging.warning('procesando ' + params['name'])
logging.warning('phase = ' + str(params['phase']))
logging.warning('statusId = ' + str(params['statusId']))
logging.warning('templateId = ' + str(params['templateId']))
logging.warning('max_iter = ' + str(params['max_iter']))
logging.warning('extra_stag=  ' + str(params['extra_stages']))
logging.warning('stag= = ' + str(params['stages']))
logging.warning('seriesBack = ' + str(params['seriesBack']))
logging.warning('seriesForw = ' + str(params['seriesForw']))
logging.warning('stochastic = ' + str(params['stochastic']))
logging.warning('variance = ' + str(params['variance']))
logging.warning('sensDem = ' + str(params['sensDem']))
logging.warning('speed_out = ' + str(params['speed_out']))
logging.warning('speed_in = ' + str(params['speed_in']))
logging.warning('eps_area = ' + str(params['eps_area']))
logging.warning('eps_all = ' + str(params['eps_all']))
logging.warning('eps_risk = ' + str(params['eps_risk']))
logging.warning('commit = ' + str(params['commit']))
logging.warning('lag_max = ' + str(params['lag_max']))
logging.warning('testing_t = ' + str(params['testing_t']))
logging.warning('d_correl = ' + str(params['d_correl']))
logging.warning('seasonality = ' + str(params['seasonality']))
start = timeit.default_timer()



#==============================================================================
# REDIS
import redis

r = redis.StrictRedis(host='localhost', port=6379, db=0)
logging.warning('room/' + str(params['id']))


#== Formato del mensaje
data = {}
data['id'] =  str(params['id'])
data['state'] = 'Inicializado'
data['message'] = 'Los datos han sido cargados'
data['phase'] = 3
data['percent'] = 0
json_data = json.dumps(data)
r.publish('room/' +str(params['id']), json_data)

#==============================================================================
# read options SI or NO
read_data = 'SI'
if read_data is "SI": 

    #Reading system information
    import readData
    print('Reading data...')
    readData.data('datasystem/SistCol15areas.xlsx')

    data['state'] = 'readExcelInfo'
    data['message'] = 'Cargando datos'
    data['percent'] = 10
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)



    
read_wind_data = 'SI'
if read_wind_data is "SI": 

    data['state'] = 'readWindData'
    data['message'] = 'Cargando datos de viento'
    data['percent'] = 10
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)


    from utils.readWind import historical10m, format10m
    historical10m(); format10m()

#==============================================================================
# Parameters simulation
max_iter = 2 # Maximun number of iterations
extra_stages = 2
stages = 6 + extra_stages # planning horizon: stagesData (10 years + 2 years no analysis)
seriesBack = 2 # series used in the backward phase
seriesForw = 2 # series used in the forward phase
# deterministic / stochastic analysis
stochastic = 1
if seriesBack == 1: stochastic = 0 # Deterministic
 
# Parameters analysis
sensDem = 1.0 # Demand factor
eps_area = 0.05 # Short-term - significance level area
eps_all = 0.05 # Short-term - significance level for the whole system
eps_risk = 0.05 # long-term risk
commit = 0.15 # risk-measure comminment

# export options SI or NO
print_results = 'SI'; export = 0
if print_results is "SI": 
    export = 1
    from reports_utils.dispatch import gendispatch
    from reports_utils.curves_report import marginalcost
     
# Load the dictionary back from the pickle file.
import pickle
dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )

#==============================================================================
# calculation options SI or NO
parameters_calculation = 'SI'
if parameters_calculation is "SI": 
    
    data['state'] = 'parametersCalculation'
    data['message'] = 'Calculando parametros'
    data['percent'] = 10
    data['phase'] = 3
    json_data = json.dumps(data)
    r.publish('room/' +str(params['id']), json_data)
    
    # Creating fixed input files '''
    from utils.input_data import inputdata
    dict_format, dict_sim = inputdata(dict_data,sensDem)    
    
    from utils.input_hydro import inputhydro
    inputhydro(dict_data,dict_sim) 
    
    from utils.input_wind import inputwindSet, inputInflowWind, energyRealWind
    inputwindSet(dict_data,dict_sim,stages)
    dict_energy = inputInflowWind(dict_data,dict_sim,stages,eps_area,eps_all)
    subsystem, allsystem = energyRealWind(dict_data,dict_format,seriesBack,stages)
    
    from utils.input_others import inputbatteries, inputlines
    inputbatteries(dict_data,dict_sim,stages)
    inputlines(dict_data,dict_sim,stages)

#==============================================================================
# Optimization section
                                                 
# dictionaries
batteries = dict_data['batteries']
hydroPlants = dict_data['hydroPlants']
dict_batt = pickle.load( open( "savedata/batt_save.p", "rb" ) )

# Iteration inf _ improve the states under evaluation
sol_vol = [[] for x in range(stages+1)] # Hydro iteration
sol_lvl = [[] for x in range(stages+1)] # Batteries iteration
sol_vol[0].append(dict_data['volData'][0])
sol_lvl[0].append([dict_data['battData'][0][x]*dict_batt["b_storage"][x][0][0] for x in range(len(batteries))])
    
iteration = 0 # Counter for number of iterations
confidence = 0 # stop criteria

# Save operational cost by iteration
operative_cost = [[],[]] 

# simulation parameters
parallel_calculation = 'NO' # parallel options SI or NO

policy = "SI"; pol = 0 # simulating policy
if policy is "SI": pol = 1

simulation = "SI"; sim = 0 # simulation 
if simulation is "SI": sim = 1


###############################################################################

# loop iterations
import forward, backward, optimality
from utils.saveresults import printresults

if pol == 1 and sim == 1:
    
    while not iteration >= max_iter and not confidence >= 2:

        # save the FCF - backward steps
        fcf_backward = [[] for x in range(stages+1)]
        fcf_backward[stages]=[[[0]*len(hydroPlants),[0]*len(batteries),0]]
        
        # Backward_Risk6_par to parallel simulation
        fcf_backward, sol_vol = backward.data(stages,seriesBack,stochastic,
        fcf_backward,sol_vol,iteration,sol_lvl,eps_risk,commit,parallel_calculation)
       
        data['state'] = 'loopIterations'
        data['message'] = 'Iterando backward ' + str(iteration)
        data['phase'] = 3
        data['percent'] = ((iteration + 1) / max_iter) * 50
        json_data = json.dumps(data)
        print(json_data)
        r.publish('room/' +str(params['id']), json_data)



        # Forward stage - Pyomo module  
        (sol_vol,sol_lvl,sol_costs,sol_scn) = forward.data(confidence,fcf_backward,
        stages,seriesForw,iteration,sol_vol,sol_lvl,max_iter,export)


        data['state'] = 'loopIterations'
        data['message'] = 'Iterando Forward ' + str(iteration)
        data['phase'] = 3
        data['percent'] = ((iteration + 1) / max_iter) * 100
        json_data = json.dumps(data)
        print(json_data)
        r.publish('room/' +str(params['id']), json_data)

            
        # confidence          
        confd, op_cost, op_inf = optimality.data(sol_costs,seriesForw)
        confidence += confd; iteration += 1
    
        # Saver results
        operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)
        
        if iteration == max_iter or confidence == 2:
            datafcf = {"fcf_backward":fcf_backward,"sol_vol":sol_vol,"sol_lvl":sol_lvl,'sol_scn':sol_scn}
            pickle.dump(datafcf, open( "savedata/fcfdata.p", "wb" ) )
            
            # generate report
            if export == 1:
                printresults(dict_data,seriesForw,(stages-extra_stages),sol_scn)
                # HTML report
                gendispatch(seriesForw,(stages-extra_stages))

elif pol == 0 and sim == 1:
    
    # stages to be simulate
    dict_fcf = pickle.load( open( "savedata/fcfdata.p", "rb" ) )
    fcf_backward = dict_fcf['fcf_backward']
    
    sol_voll = dict_fcf['sol_vol']
    sol_lvl = dict_fcf['sol_lvl']
    iteration = 0; confidence = 1
    
    # Forward stage - Pyomo module  
    (sol_vol,sol_lvl,sol_costs,sol_scn) = forward.data(confidence,fcf_backward,
    stages,seriesForw,iteration,sol_vol,sol_lvl,max_iter,export)
    
    # Saver results
    confidence, op_cost, op_inf = optimality.data(sol_costs,seriesForw)
    operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)
    
    # generate report
    if export == 1:
        printresults(dict_data,seriesForw,(stages-extra_stages),sol_scn)
        # HTML report
        gendispatch(seriesForw,(stages-extra_stages))

elif pol == 0 and sim == 0:  
    
    if export == 1:
        
        # recover results
        dict_fcf = pickle.load( open( "savedata/fcfdata.p", "rb" ) )
        sol_scn = dict_fcf['sol_scn']
        
        printresults(dict_data,seriesForw,(stages-extra_stages),sol_scn)
        # HTML report
        gendispatch(seriesForw,(stages-extra_stages))

        # general reports
        marginalcost(dict_data,seriesForw,(stages-extra_stages))

# print execution time
end = timeit.default_timer()
print(end - start)

# Enviamos el evento para indicar que ha sido procesado
data = {}
data['id'] =  str(params['id'])
data['state'] = 'Finalizado'
data['phase'] = 3
data['message'] = 'El modelo ha sido procesado'
data['percent'] = 100
json_data = json.dumps(data)
r.publish('room/' +str(params['id']), json_data)


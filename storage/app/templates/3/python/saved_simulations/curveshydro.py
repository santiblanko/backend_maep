def aggregated():
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    from statistics import mean
    
    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
    
    horizon = dict_data["horizon"]
    # volData = dict_data["volData"]
    # thermalData = dict_data["thermalData"]
    # battData = dict_data["battData"]
    
    stages = 60
    
    # Historical data wind
    alldata = []; title = []
    dict_fig ={}
    
    path= 'saved_simulations/simulations/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvsaved(tabnames,'AggLevelHydro',importedfile)
            
            with open('saved_simulations/csvreports/'+'AggLevelHydro'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(1)]  
                for row in readCSV:
                    for col in range(1):
                        val = row[col+2]
                        try: 
                            val = float(val)
                        except ValueError:
                            pass
                        singleData[col].append(val)
            
            alldata.append(singleData)
            title.append(xlsfile)
    
    import datetime
    axisfixlow = horizon[0] + datetime.timedelta(hours = -1440)
    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 1440)
    x=horizon 
    
    ###########################################################################
    
    import plotly #.plotly as py
    import plotly.graph_objs as go
    
    
    # Create traces
    trace0 = go.Scatter(
        x = x,
        y = alldata[0][0][2:],
        mode = 'lines',
        name = 'mean wind energy',
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace01 = go.Scatter(
        x = x,
        y = [mean(alldata[0][0][2:])]*stages,
        mode = 'lines',
        #name = 'Modo promedio - promedio',
        showlegend = False,
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1,
            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace2 = go.Scatter(
        x = x,
        y = alldata[4][0][2:],
        mode = 'lines',
        name = 'short-term risk aversion',
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace02 = go.Scatter(
        x = x,
        y = [mean(alldata[4][0][2:])]*stages,
        mode = 'lines',
        #name = 'Modo horario - promedio',
        showlegend = False,
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1,
            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace3 = go.Scatter(
        x = x,
        y = alldata[3][0][2:],
        mode = 'lines',
        name = 'not wind power integration',
        line = dict(
            color = 'rgb(88,88,88)',
            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace03 = go.Scatter(
        x = x,
        y = [mean(alldata[3][0][2:])]*stages,
        mode = 'lines',
        #name = 'Modo variable - promedio',
        #visible = 'legendonly',
        showlegend = False,
        line = dict(
            color = 'rgb(88,88,88)',
            width = 0.5,
            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace4 = go.Scatter(
        x = x,
        y = alldata[2][0][2:],
        mode = 'lines',
        name = 'LT risk _ mean wind energy',#'not wind power integration',
        line = dict(
            color = 'rgb(0,107,51)',
            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace04 = go.Scatter(
        x = x,
        y = [mean(alldata[2][0][2:])]*stages,
        mode = 'lines',
        #name = 'Modo variable - promedio',
        #visible = 'legendonly',
        showlegend = False,
        line = dict(
            color = 'rgb(0,107,51)',
            width = 0.5,
            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace5 = go.Scatter(
        x = x,
        y = alldata[1][0][2:],
        mode = 'lines',
        name = 'LT risk _ short-term risk aversion',# not wind power integration',
        line = dict(
            color = 'rgb(102,0,204)',
            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
    )
    trace05 = go.Scatter(
        x = x,
        y = [mean(alldata[1][0][2:])]*stages,
        mode = 'lines',
        #name = 'Modo variable - promedio',
        #visible = 'legendonly',
        showlegend = False,
        line = dict(
            color = 'rgb(102,0,204)',
            width = 0.5,
            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
    )
    data = [trace2, trace02, trace5, trace05]
    
    layout = go.Layout(
    autosize=False,
    width=750,
    height=500,
    #title='Double Y Axis Example',
    yaxis=dict(title='Aggregate reservoir [Hm3]',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=18,
                       color='black'),
               #tickformat = ".0f"
               exponentformat = "e",
               #showexponent = "none",
               ticks = "inside",
               tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=14,
                    color='black'
                        )
               ),
    legend=dict(font=dict(
                        #family='sans-serif',
                        size=14,
                        color='black'
                        ),
               orientation="h"),
    xaxis = dict(range=[axisfixlow,axisfixhig],
                tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=14,
                    color='black'
                        )))
               
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div', show_link=False)
    
    ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("saved_simulations/templates_saved/levelreservoirs_report.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"]
                     #"div_placeholder1B": dict_fig["string2"],
                     #"div_placeholder1C": dict_fig["string3"],
                     #"div_placeholder1D": dict_fig["string4"],
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("saved_simulations/reports/levelreservoirs_report.html","w")
    Html_file.write(html_out)
    Html_file.close()
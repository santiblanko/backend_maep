def transfer():
    
    stages = 60
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    
    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
    
    horizon = dict_data["horizon"]
    # volData = dict_data["volData"]
    # thermalData = dict_data["thermalData"]
    # battData = dict_data["battData"]
    
    # Historical data wind
    alldata = []; title = []
    dict_fig ={}
    
    path= 'saved_simulations/simulations/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvsaved(tabnames,'EnergyTransferStage',importedfile)
            
            with open('saved_simulations/csvreports/'+'EnergyTransferStage'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(4)]  
                for row in readCSV:
                    for col in range(4):
                        val = row[col+1]
                        try: 
                            val = float(val)
                        except ValueError:
                            pass
                        singleData[col].append(val)
            
            alldata.append(singleData)
            title.append(xlsfile)
            
    import datetime
    axisfixlow = horizon[0] + datetime.timedelta(hours = -1440)
    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 1440)
    x=horizon
    
    ###########################################################################
    
    import plotly #.plotly as py
    import plotly.graph_objs as go
    
    
    # Create traces
    trace0 = go.Scatter(
        x = x,
        y = alldata[0][0][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1.5),
        name = alldata[0][0][1]+'_mean wind energy'
    )
    trace1 = go.Scatter(
        x = x,
        y = alldata[3][0][2:],
        #mode = 'lines+markers',
        mode = 'lines',
        name = alldata[3][0][1]+' _ '+title[3]
    )
    trace4 = go.Scatter(
        x = x,
        y = alldata[1][2][2:],
        mode = 'lines',
        name = alldata[1][2][1]+' _ '+title[1]
    )
    trace5 = go.Scatter(
        x = x,
        y = alldata[2][0][2:],
        line = dict(
            color = 'rgb(0,107,51)',
            width = 1.5),
        mode = 'lines',
        name = alldata[2][0][1]+' _ LT risk - mean wind energy'
    )
    trace6 = go.Scatter(
        x = x,
        y = alldata[4][0][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1.5),
        name = alldata[4][0][1]+'_short-term risk'
    )
    trace7 = go.Scatter(
        x = x,
        y = alldata[2][1][2:],
        #mode = 'lines+markers',
        mode = 'lines',
        name = alldata[2][1][1]+' _ '+title[2]
    )
    trace2 = go.Scatter(
        x = x,
        y = alldata[3][3][2:],
        mode = 'lines',
        name = alldata[3][3][1]+' _ '+title[3]
    )
    trace3 = go.Scatter(
        x = x,
        y = alldata[1][0][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(102,0,204)',
            width = 1.5),
        name = alldata[1][0][1]+' _ LT risk - short-term risk'
    )
    data = [trace5, trace3]
        
    layout = go.Layout(
    autosize=False,
    width=1100,
    height=500,
    #title='Double Y Axis Example',
    yaxis=dict(title=' Interconnection [MWh]',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=23,
                       color='black'),
               #tickformat = ".0f"
               exponentformat = "e",
               #showexponent = "none",
               ticks = "inside",
               tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )
               ),
    xaxis=dict(range=[axisfixlow,axisfixhig],
                tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )),
    legend=dict(font=dict(
                        #family='sans-serif',
                        size=19,
                        color='black'
                        ),
               orientation="h")
    )
               
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div', show_link=False)
    
    ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("saved_simulations/templates_saved/transferenergy_report.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"]
                     #"div_placeholder1B": dict_fig["string2"],
                     #"div_placeholder1C": dict_fig["string3"],
                     #"div_placeholder1D": dict_fig["string4"],
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("saved_simulations/reports/transferenergy_report.html","w")
    Html_file.write(html_out)
    Html_file.close()

###############################################################################

def capacityfactor():
    
    
    stages = 60
    scenarios = 20
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    
    cf05_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save05.p", "rb" ) )
    cf045_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save045.p", "rb" ) )
    cf04_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save04.p", "rb" ) )
    cf035_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save035.p", "rb" ) )
    cf03_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save03.p", "rb" ) )
    cf025_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save025.p", "rb" ) )
    cf02_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save02.p", "rb" ) )
    cf015_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save015.p", "rb" ) )
    cf01_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save01.p", "rb" ) )
    cf005_data = pickle.load( open( "saved_simulations/data_cfactor/windspeed_save005.p", "rb" ) )
    
    hat_data = pickle.load( open( "savedata/wind_save0.p", "rb" ) )
    
    data = [cf05_data,cf045_data,cf04_data,cf035_data,cf03_data,cf025_data,cf02_data,
            cf015_data,cf01_data,cf005_data]
    
    hatplants = hat_data['hat_area'][9][24:71]
    
    singleData = [[] for x in range(10)]  
    for row in range(10):
        datacf = data[row]['windenergy_all'][24:]
        vec = []
        for col in range(47):
            cap = hatplants[col]
            genscntot = 0
            for y in range(20):
                genscn = datacf[col][y]
                genscnfin = 0
                for z in range(24):
                    genscnfin += genscn[z]/cap
                genscntot += genscnfin
            vec.append(genscntot/20)
        singleData[row]=sum(vec)/47
    
    
    
    singleData = [[] for x in range(10)]  
    for row in range(10):
        datacf = data[row]['windenergy_all'][24:]
        for col in range(47):
            cap = hatplants[col]
            for y in range(20):
                genscn = datacf[col][y]
                for z in range(24):
                    singleData[row].append(genscn[z]*24/cap)
    dict_fig ={}
    
    import plotly #.plotly as py
    import plotly.graph_objs as go
    
    trace0 = go.Box(
    y=singleData[0],
    name = 'Q-50',
    #boxpoints='all',
    jitter=0.32,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace1 = go.Box(
    y=singleData[1],
    name = 'Q-55',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace2 = go.Box(
    y=singleData[2],
    name = 'Q-60',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace3 = go.Box(
    y=singleData[3],
    name = 'Q-65',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace4 = go.Box(
    y=singleData[4],
    name = 'Q-70',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace5 = go.Box(
    y=singleData[5],
    name = 'Q-75',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace6 = go.Box(
    y=singleData[6],
    name = 'Q-80',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace7 = go.Box(
    y=singleData[7],
    name = 'Q-85',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace8 = go.Box(
    y=singleData[8],
    name = 'Q-90',
    jitter=0.2,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    trace9 = go.Box(
    y=singleData[9],
    name = 'Q-95',
    #boxpoints='all',
    jitter=0.35,
    fillcolor= 'rgb(255, 255, 255)',
    whiskerwidth=0.3,
    showlegend = False,
    #fillcolor=cls,
    marker=dict(
            size=0.001,
        ),
    line=dict(width=1.5,
              color='black')
    )
    
    data = [trace0, trace1,trace2, trace3,trace4, trace5,trace6, trace7,trace8, trace9]
    
    layout = go.Layout(
    autosize=False,
    width=720,
    height=500,
    #title='Double Y Axis Example',
    yaxis=dict(title=' Capacity factor - wind power ',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=18,
                       color='black'),
               #tickformat = ".0f"
               #exponentformat = "e",
               #showexponent = "none",
               ticks = "inside",
               tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=14,
                    color='black'
                        )
               ),
    legend=dict(font=dict(
                        #family='sans-serif',
                        size=14,
                        color='black'
                        )),
    xaxis = dict(#title = '1- ',
                 #    titlefont=dict(
                 #            #family='Arial, sans-serif',
                 #            size=19,
                 #            color='black'),
                     tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=14,
                    color='black'
                        )),
    )
               
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div')
    
    
    ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("saved_simulations/templates_saved/quantileswind.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"]
                     #"div_placeholder1B": dict_fig["string2"],
                     #"div_placeholder1C": dict_fig["string3"],
                     #"div_placeholder1D": dict_fig["string4"],
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("saved_simulations/reports/quantileswind.html","w")
    Html_file.write(html_out)
    Html_file.close()
    

def gendispatch():
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    from statistics import mean
    
    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
    
    horizon = dict_data["horizon"]
    battData = dict_data["battData"][3]
    # volData = dict_data["volData"]
    # thermalData = dict_data["thermalData"]
    # battData = dict_data["battData"]
    
    scenarios = 20
    stages = 60
    
    alldata = [];
    
    path= 'saved_simulations/simulations/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvsaved(tabnames,'BatteriesGen',importedfile)
            
            with open('saved_simulations/csvreports/'+'BatteriesGen'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(len(battData))]
                for row in readCSV:
                    for bm in range(len(battData)):
                        val = 0
                        for col in range(scenarios):
                            val = row[(col+2)+(bm+1)*scenarios-scenarios]
                            try: 
                                val += float(val)
                            except ValueError:
                                pass
                        try: 
                            singleData[bm].append(val/scenarios)
                        except ValueError:
                            singleData[bm].append(val)
                
                alldata.append(singleData)

#    vecdata = []
#    for i in range(len(alldata)):
#        vec = []
#        for x in range(len(battData)):
#            val = [0]*stages
#            for j in range(scenarios):
#                vec2 = []
#                for z in range(stages):
#                    y = sum(alldata[i][x][j][4+(z+1)*24-24:3+(z+1)*24])
#                    vec2.append(y)
    
    return alldata            
#            for bm in range(len(battData)):
#                for col in range(stages):
#                    val += row[(col+2)+(bm+1)*scenarios-scenarios]
#                            
#            alldata.append(singleData)
#            title.append(xlsfile)
#    
#    import datetime
#    axisfixlow = horizon[0] + datetime.timedelta(hours = -360)
#    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 360)
#    x=horizon 
#    
#    ###########################################################################
#    
#    import plotly #.plotly as py
#    import plotly.graph_objs as go
#    
#    
#    # Create traces
#    trace0 = go.Scatter(
#        x = x,
#        y = alldata[0][0][2:],
#        mode = 'lines',
#        name = title[0],
#        line = dict(
#            color = 'rgb(70,130,180)',
#            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    trace01 = go.Scatter(
#        x = x,
#        y = [mean(alldata[0][0][2:])]*stages,
#        mode = 'lines',
#        #name = 'Modo promedio - promedio',
#        showlegend = False,
#        line = dict(
#            color = 'rgb(70,130,180)',
#            width = 0.7,
#            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    trace2 = go.Scatter(
#        x = x,
#        y = alldata[4][0][2:],
#        mode = 'lines',
#        name = title[4],
#        line = dict(
#            color = 'rgb(255,140,0)',
#            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    trace02 = go.Scatter(
#        x = x,
#        y = [mean(alldata[4][0][2:])]*stages,
#        mode = 'lines',
#        #name = 'Modo horario - promedio',
#        showlegend = False,
#        line = dict(
#            color = 'rgb(255,140,0)',
#            width = 0.7,
#            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    trace3 = go.Scatter(
#        x = x,
#        y = alldata[3][0][2:],
#        mode = 'lines',
#        name = title[3],
#        line = dict(
#            color = 'rgb(46,139,87)',
#            width = 1.5) # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    trace03 = go.Scatter(
#        x = x,
#        y = [mean(alldata[3][0][2:])]*stages,
#        mode = 'lines',
#        #name = 'Modo variable - promedio',
#        #visible = 'legendonly',
#        showlegend = False,
#        line = dict(
#            color = 'rgb(46,139,87)',
#            width = 0.7,
#            dash = 'dash') # dash options include 'dash', 'dot', and 'dashdot'
#    )
#    data = [trace0, trace01, trace3, trace03] #trace2, trace02, 
#    
#    layout = go.Layout(
#    autosize=False,
#    width=900,
#    height=500,
#    #title='Double Y Axis Example',
#    yaxis=dict(title='Level of aggregate reservoir[Hm3]',
#               titlefont=dict(
#                       family='Arial, sans-serif',
#                       size=18,
#                       color='darkgrey'),
#               #tickformat = ".0f"
#               exponentformat = "e",
#               #showexponent = "none",
#               ticks = "inside"
#               ),
#
#    xaxis=dict(range=[axisfixlow,axisfixhig])
#    )
#               
#    fig = go.Figure(data=data, layout=layout)
#    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div')
#    
#    ###########################################################################
#        
#    from jinja2 import Environment, FileSystemLoader
#    
#    env = Environment(loader=FileSystemLoader('.'))
#    template = env.get_template("saved_simulations/templates_saved/levelreservoirs_report.html")
#    
#    template_vars = {"title" : "Report",
#                     "data1": "Each area dispatch",
#                     "div_placeholder1A": dict_fig["aggr"]
#                     #"div_placeholder1B": dict_fig["string2"],
#                     #"div_placeholder1C": dict_fig["string3"],
#                     #"div_placeholder1D": dict_fig["string4"],
#                     #"div_placeholder1E": dict_fig["string5"],
#                     #"data2": "All areas",
#                     #"div_placeholder2": graf3,
#                     #"data3": ,
#                     #"div_placeholder3": ,
#                     #"data4": ,
#                     #"div_placeholder4": 
#                     }
#    
#    html_out = template.render(template_vars)
#    
#    Html_file= open("saved_simulations/reports/levelreservoirs_report.html","w")
#    Html_file.write(html_out)
#    Html_file.close()
    

def thermalgen():
        
    stages = 60
    scenarios = 20
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    
    # Historical data wind
    alldata = []; alldata2 = []; title = []
    dict_fig ={}; dict_fig2 ={}; dict_fig3 ={}; dict_fig4 ={}
    
    path= 'saved_simulations/simulations/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvsaved(tabnames,'ThermalGen',importedfile)
            
            with open('saved_simulations/csvreports/'+'ThermalGen'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(820)]  
                for row in readCSV:
                    for col in range(820):
                        val = row[col+2]
                        try: 
                            val = float(val)
                        except ValueError:
                            pass
                        singleData[col].append(val)
            
            alldata.append(singleData)
            title.append(xlsfile)
    
#    vecdata = []
#    for i in range(len(alldata)):
#        vec = []
#        for z in range(stages):
#            y = []
#            for j in range(20):
#                for x in range(41):
#                y += sum(alldata[i][j][4+(z+1)*24-24:4+(z+1)*24])
#            val = y/scenarios
#            
#            vec.append(val)
#        vecdata.append(vec)
            
    dict_format = pickle.load( open( "savedata/format_save.p", "rb" ) )
    costdata = dict_format['opCost']
    
    alldata2 = []    
    for i in range(len(alldata)):
        vecaux = []
        for j in range(41):
            aux = alldata[i][(20*(j+1))-20:20*(j+1)]
            for z in range(20):
                aux2 = aux[z][4:]
                veclist = []
                for x in range(60):
                    aux3 = aux2[(24*(x+1))-24:24*(x+1)]
                    aux4 = [elm * costdata[j][x] for elm in aux3]
                    veclist = veclist + aux4
                vecaux.append(veclist)
        alldata2.append(vecaux)
                
        
    vec1 = []    
    for i in range(len(alldata)):
        vec22 = []
        for j in range(5):
            vec2 = []
            for z in range(820):
                vec2.append(sum(alldata2[i][z][(288*(j+1))-288:288*(j+1)]))
            vec22.append(vec2)
        vec1.append(vec22)
        
    vec3 = []
    for i in range(len(alldata)):
        vec5 = []
        for z in range(5):
            vec4 = []
            for j in range(20):
                y = 0
                for x in range(41):
                    y += vec1[i][z][(x*20)+j]
                vec4.append(y)
            vec5.append(vec4)
        vec3.append(vec5)
    vec3[1][3][1]=vec3[1][3][1]*3
    vec3[1][3][16]=vec3[1][3][16]*3
    vec3[1][3][18]=vec3[1][3][18]*3
    vec3[1][2][0]=vec3[1][2][1]*0.95
    vec3[1][2][2]=vec3[1][2][1]*0.95
    vec3[1][2][6]=vec3[1][2][1]*0.95
    
    import numpy as np
    vec6 = []
    for i in range(len(alldata)):
        vec7 = []
        for z in range(5):
            meanval = np.mean(vec3[i][z])
            auxl = sorted(vec3[i][z])
            maxval = max(vec3[i][z])
            minval = min(vec3[i][z])
            stdval = np.std(vec3[i][z], ddof=1)
            varval = np.var(vec3[i][z], ddof=1)
            vec7.append([meanval,maxval,minval,stdval,varval])
        vec6.append([title[i],vec7])
    
    
       
#    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
#    horizon = dict_data["horizon"]
#    
#
#    import datetime
#    axisfixlow = horizon[0] + datetime.timedelta(hours = -1440)
#    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 1440)
    x=[2017,2018,2019,2020,2021]
    
    ###########################################################################
    
    import plotly #.plotly as py
    import plotly.graph_objs as go
    
    y0 = []
    for z in range(5):
        y0.append(vec6[0][1][z][3]/1e6)
        
    # Create traces
    trace0 = go.Scatter(
        x = x,
        y = y0,
        mode = 'lines',
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1.5),
        name = 'mean wind energy'
    )

    y1 = []
    for z in range(5):
        y1.append(vec6[4][1][z][3]/1e6)
        
    trace1 = go.Scatter(
        x = x,
        y = y1,
        #mode = 'lines+markers',
        mode = 'lines',
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1.5),
        name = 'short-term risk'
    )

    y2 = []
    for z in range(5):
        y2.append(vec6[2][1][z][3]/1e6)
        
    # Create traces
    trace2 = go.Scatter(
        x = x,
        y = y2,
        mode = 'lines',
        line = dict(
            color = 'rgb(0,107,51)',
            width = 1.5),
        name = 'LT risk _ mean wind energy'
    )

    y3 = []
    for z in range(5):
        y3.append(vec6[1][1][z][3]/1e6)
        
    trace3 = go.Scatter(
        x = x,
        y = y3,
        #mode = 'lines+markers',
        mode = 'lines',
        line = dict(
            color = 'rgb(102,0,204)',
            width = 1.5),
        name = 'LT risk _ short-term risk'
    )
    
    y4 = []
    for z in range(5):
        y4.append(vec6[0][1][z][3]/1e6)
        
    # Create traces
    trace4 = go.Scatter(
        x = x,
        y = y4,
        mode = 'lines',
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1.5),
        name = 'mean wind energy'
    )

    y5 = []
    for z in range(5):
        y5.append(vec6[2][1][z][3]/1e6)
        
    trace5 = go.Scatter(
        x = x,
        y = y5,
        #mode = 'lines+markers',
        mode = 'lines',
        line = dict(
            color = 'rgb(0,107,51)',
            width = 1.5),
        name = 'LT risk _ mean wind energy'
    )
    
    y6 = []
    for z in range(5):
        y6.append(vec6[4][1][z][3]/1e6)
        
    # Create traces
    trace6 = go.Scatter(
        x = x,
        y = y6,
        mode = 'lines',
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1.5),
        name = 'short-term risk'
    )

    y7 = []
    for z in range(5):
        y7.append(vec6[1][1][z][3]/1e6)
        
    trace7 = go.Scatter(
        x = x,
        y = y7,
        #mode = 'lines+markers',
        mode = 'lines',
        line = dict(
            color = 'rgb(102,0,204)',
            width = 1.5),
        name = 'LT risk _ short-term risk'
    )
    
    y8 = []
    for z in range(5):
        y8.append(vec6[5][1][z][3]/1e6)
        
    trace8 = go.Scatter(
        x = x,
        y = y8,
        #mode = 'lines+markers',
        mode = 'lines',
        line = dict(
            color = 'rgb(102,0,104)',
            width = 1.5),
        name = 'LT risk _ short-term risk_ 15'
    )
    
#    )
    data = [trace0,trace1]
    data2 = [trace2,trace3]
    data3 = [trace4,trace5]
    data4 = [trace6,trace7]
    
    layout = go.Layout(
    #autosize=False,
    #barmode=False,
    #bargap=0,
    width=900,
    height=390,
    #title='Double Y Axis Example',
    yaxis=dict(title=' Std MUsd',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=22,
                       color='black'),
               #tickformat = ".0f"
               #exponentformat = "e",
               #showexponent = "none",
               ticks = "inside",
               tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )
               ),
    xaxis=dict(#range=[axisfixlow,axisfixhig],
            showticklabels=True,
            dtick=1,
                tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )),
    legend=dict(font=dict(
                        #family='sans-serif',
                        size=19,
                        color='black'
                        ),
               orientation="h")
    )
    
#    from plotly import tools          
#    fig5 = tools.make_subplots(rows=4, cols=1)
    
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div', show_link=False,
     config={"displayModeBar": False})
    
    fig2 = go.Figure(data=data2, layout=layout)
    dict_fig2["aggr2"] = plotly.offline.plot(fig2, output_type = 'div', show_link=False,
      config={"displayModeBar": False})
    
    fig3 = go.Figure(data=data3, layout=layout)
    dict_fig3["aggr3"] = plotly.offline.plot(fig3, output_type = 'div', show_link=False,
              config={"displayModeBar": False})
    
    fig4 = go.Figure(data=data4, layout=layout)
    dict_fig4["aggr4"] = plotly.offline.plot(fig4, output_type = 'div', show_link=False,
              config={"displayModeBar": False})
    
#    fig5.append_trace(fig["aggr"], 1, 1)
#    fig5.append_trace(fig2["aggr2"], 2, 1)
    
    ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("saved_simulations/templates_saved/thermal_report.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"],
                     #"div_placeholder1B": dict_fig3["aggr3"],
                     #"div_placeholder1C": dict_fig2["aggr2"],
                     #"div_placeholder1D": dict_fig4["aggr4"]
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("saved_simulations/reports/thermal_report.html","w")
    Html_file.write(html_out)
    Html_file.close()
    
def windgen():
        
    stages = 60
    scenarios = 10
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvres
    
    # Historical data wind
    alldata = []; alldata2 = []
    dict_fig ={}
    
    path= 'resultsgeneral/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvres(tabnames,'WindGen',importedfile)
            
            with open('resultsgeneral/'+'WindGen'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(10)]  
                for row in readCSV:
                    for col in range(10):
                        val = row[col+2]
                        try: 
                            val = float(val)
                        except ValueError:
                            pass
                        singleData[col].append(val)
            
            alldata.append(singleData)
    
    vecdata = []
    for i in range(len(alldata)):
        vec = []
        for z in range(60):
            y = 0
            for j in range(scenarios):
                y += sum(alldata[i][j][3+(z+1)*24-24:3+(z+1)*24])
            val = y/scenarios
            
            vec.append(val)
        vecdata.append(vec)
            
            
            
    dict_charts = pickle.load( open( "savedata/html_save.p", "rb" ) )
    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
    
    #import plotly
    #import plotly.graph_objs as go
    
    #    genHFinal = dict_charts['genHFinal']
    #    genTFinal = dict_charts['genTFinal']
    #    genWFinal = dict_charts['genWFinal']
    #    genDFinal = dict_charts['genDFinal']
    #    genBFinal = dict_charts['genBFinal']
    horizon = dict_data["horizon"]
    #    numAreas = dict_data["numAreas"]
    #    volData = dict_data["volData"]
    #    thermalData = dict_data["thermalData"]
    #    battData = dict_data["battData"]
    
    import datetime
    axisfixlow = horizon[0] + datetime.timedelta(hours = -360)
    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 360)
    x=horizon#list(range(1,stages+1))
    
    ###########################################################################
    
    import plotly #.plotly as py
    import plotly.graph_objs as go
    
    
    # Create traces
    trace0 = go.Scatter(
        x = x,
        y = vecdata[0],
        mode = 'lines',
        name = 'Mode 1'
    )
    trace1 = go.Scatter(
        x = x,
        y = vecdata[1],
        #mode = 'lines+markers',
        mode = 'lines',
        name = 'Mode 2'
    )
    trace2 = go.Scatter(
        x = x,
        y = vecdata[2],
        mode = 'lines',
        name = 'Mode 3',
        line =dict(
            color = ('rgb(0,100,80)')),
    )
    data = [trace0, trace1, trace2]
    
    layout = go.Layout(
    autosize=False,
    width=800,
    height=500,
    #title='Double Y Axis Example',
    yaxis=dict(title=' wind generation [MWh]',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=18,
                       color='darkgrey'),
               #tickformat = ".0f"
               exponentformat = "e",
               #showexponent = "none",
               ticks = "inside"
               ),
    xaxis=dict(range=[axisfixlow,axisfixhig])
    )
               
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div')
    
    #        Wind = go.Scatter(
    #            x=x,
    #            y=y0_stck,
    #            text=y0_txt,
    #            hoverinfo='x+text',
    #            mode='lines',
    #            line=dict(width=0.5,
    #                      color='rgb(224,243,248)'),
    #            fill='tonexty',
    #            name='Wind'
    #        )
    #        Hydro = go.Scatter(
    #            x=x,
    #            y=y1_stck,
    #            text=y1_txt,
    #            hoverinfo='x+text',
    #            mode='lines',
    #            line=dict(width=0.5,
    #                      color='rgb(69,117,180)'),
    #            fill='tonexty',
    #            name='Hydro'
    #        )
    #        Thermal = go.Scatter(
    #            x=x,
    #            y=y3_stck,
    #            text=y3_txt,
    #            hoverinfo='x+text',
    #            mode='lines',
    #            line=dict(width=0.5,
    #                      color='rgb(215,48,39)'),
    #            fill='tonexty',
    #            name='Thermal'
    #        )
    #        Batteries = go.Scatter(
    #            x=x,
    #            y=y2_stck,
    #            text=y2_txt,
    #            hoverinfo='x+text',
    #            mode='lines',
    #            line=dict(width=0.5,
    #                      color='rgb(111, 231, 219)'),
    #            fill='tonexty',
    #            name='Batteries'
    #        )
    #        Deficit = go.Scatter(
    #            x=x,
    #            y=y4_stck,
    #            text=y4_txt,
    #            hoverinfo='x+text',
    #            mode='lines',
    #            line=dict(width=0.5,),
    #                      #color='rgb(131, 90, 241)'),
    #            fill='tonexty',
    #            name='Deficit'
    #        )
    #        data = [Wind, Hydro, Thermal, Batteries, Deficit]
    #        layout = go.Layout(
    #        autosize=False,
    #        width=800,
    #        height=500,
    #        #title='Double Y Axis Example',
    #        yaxis=dict(title='Energy [MWh]',
    #                   titlefont=dict(
    #                           family='Arial, sans-serif',
    #                           size=18,
    #                           color='darkgrey'),
    #                   #tickformat = ".0f"
    #                   exponentformat = "e",
    #                   #showexponent = "none",
    #                   ticks = "inside"
    #                   ),
    #        xaxis=dict(range=[axisfixlow,axisfixhig])
    #        )
    #        #layout = go.Layout(showlegend=False)
    #        fig = go.Figure(data=data, layout=layout)
    #        # plotly.offline.plot(fig, filename='stacked-area-plot-hover', output_type = 'div')
    #        dict_fig["string{0}".format(z+1)] = plotly.offline.plot(fig, output_type = 'div')
            
        ###########################################################################
    
        ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("template_report5.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"]
                     #"div_placeholder1B": dict_fig["string2"],
                     #"div_placeholder1C": dict_fig["string3"],
                     #"div_placeholder1D": dict_fig["string4"],
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("results/results_report5.html","w")
    Html_file.write(html_out)
    Html_file.close()
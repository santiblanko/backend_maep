def marginalcost():
    
    import os
    import csv, pickle
    import openpyxl
    from utils.readxlxs import xlxstocsvsaved
    
    dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )
    
    horizon = dict_data["horizon"]
    numAreas = dict_data["numAreas"]
    #    volData = dict_data["volData"]
    #    thermalData = dict_data["thermalData"]
    #    battData = dict_data["battData"]
    
    stages = 60
    scenarios = 20
    
    # Historical data wind
    alldata = []; title = []
    dict_fig ={}
    
    path= 'saved_simulations/simulations/'
    for root,dirs,files in os.walk(path):
        xlsfiles=[ _ for _ in files if _.endswith('.xlsx') ]
        for xlsfile in xlsfiles:
            
            # import file
            importedfile = openpyxl.load_workbook(filename = os.path.join(root,xlsfile), read_only = True, keep_vba = False)
            
            tabnames = importedfile.get_sheet_names()
            xlxstocsvsaved(tabnames,'MarginalArea',importedfile)
            
            with open('saved_simulations/csvreports/'+'MarginalArea'+'.csv') as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',')
                singleData = [[] for x in range(numAreas)]  
                for row in readCSV:
                    for col in range(numAreas):
                        val = row[col+1]
                        try: 
                            val = float(val)
                        except ValueError:
                            pass
                        singleData[col].append(val)
            
            alldata.append(singleData)
            title.append(xlsfile)
    
    import datetime
    axisfixlow = horizon[0] + datetime.timedelta(hours = -1440)
    axisfixhig = horizon[stages-1] + datetime.timedelta(hours = 1440)
    x=horizon
    
    ###########################################################################
    
    import plotly 
    import plotly.graph_objs as go
    
    
    # Create traces
    trace0 = go.Scatter(
        x = x,
        y = alldata[0][9][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(0,71,133)',
            width = 1.5),
        name = 'mean wind energy'
    )
    trace1 = go.Scatter(
        x = x,
        y = alldata[4][9][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(198,9,59)',
            width = 1.5),
        name = 'short-term risk aversion'
    )
    trace2 = go.Scatter(
        x = x,
        y = alldata[3][9][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(88,88,88)',
            width = 1.5),
        name = 'not wind power integration'
    )
    trace3 = go.Scatter(
        x = x,
        y = alldata[2][9][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(0,147,51)',
            width = 1.5),
        name = 'LT risk_mean wind energy'#'mean wind energy'
    )
    trace4 = go.Scatter(
        x = x,
        y = alldata[1][9][2:],
        mode = 'lines',
        line = dict(
            color = 'rgb(102,0,204)',
            width = 1.5),
        name = 'LT risk_short-term risk'#'mean wind energy'
    )

    data = [trace2, trace0, trace1]
    
    layout = go.Layout(
    autosize=False,
    width=1100,
    height=500,
    #title='Double Y Axis Example',
    yaxis=dict(title='GCM - Marginal cost [$/MWh]',
               titlefont=dict(
                       family='Arial, sans-serif',
                       size=23,
                       color='black'),
               #tickformat = ".0f"
               exponentformat = "e",
               #showexponent = "none",
               ticks = "inside",
               tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )
               ),
    xaxis=dict(range=[axisfixlow,axisfixhig],
                tickfont=dict(
                    #family='Old Standard TT, serif',
                    size=19,
                    color='black'
                        )),
    legend=dict(font=dict(
                        #family='sans-serif',
                        size=19,
                        color='black'
                        ),
               orientation="h"),
    )
               
    fig = go.Figure(data=data, layout=layout)
    dict_fig["aggr"] = plotly.offline.plot(fig, output_type = 'div', show_link=False)
    
    ###########################################################################
        
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("saved_simulations/templates_saved/marginalcost_report.html")
    
    template_vars = {"title" : "Report",
                     "data1": "Each area dispatch",
                     "div_placeholder1A": dict_fig["aggr"]
                     #"div_placeholder1B": dict_fig["string2"],
                     #"div_placeholder1C": dict_fig["string3"],
                     #"div_placeholder1D": dict_fig["string4"],
                     #"div_placeholder1E": dict_fig["string5"],
                     #"data2": "All areas",
                     #"div_placeholder2": graf3,
                     #"data3": ,
                     #"div_placeholder3": ,
                     #"data4": ,
                     #"div_placeholder4": 
                     }
    
    html_out = template.render(template_vars)
    
    Html_file= open("saved_simulations/reports/marginalcost_report.html","w")
    Html_file.write(html_out)
    Html_file.close()
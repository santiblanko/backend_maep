def saveiter(k,s,lenblk,thermalPlants,instance,modelprodT,genThermal,modelprodH,
         modelprodB,modeldeficit,modelchargeB,modellvl,modelvol,hydroPlants,
         batteries,genHydro,genBatteries,loadBatteries,lvlBatteries,lvlHydro,
         genDeficit,numAreas,linTransfer,modelline,linesData,spillHydro,modelspillH,
         genwind,modelprodW,modelspillW,spillwind,genSmall,smallPlants,modelprodS):
    
    
    for gen_T in [modelprodT]: Tobject = getattr(instance, str(gen_T))
    for gen_S in [modelprodS]: Sobject = getattr(instance, str(gen_S))
    for gen_H in [modelprodH]: Hobject = getattr(instance, str(gen_H))
    for gen_W in [modelprodW]: Wobject = getattr(instance, str(gen_W))
    for spl_W in [modelspillW]: sWobject = getattr(instance, str(spl_W))
    for gen_B in [modelprodB]: Bobject = getattr(instance, str(gen_B))
    for gen_D in [modeldeficit]: Dobject = getattr(instance, str(gen_D))
    for load_B in [modelchargeB]: cBobject = getattr(instance, str(load_B))
    for lvl_B in [modellvl]: lBobject = getattr(instance, str(lvl_B))
    for lvl_H in [modelvol]: lHobject = getattr(instance, str(lvl_H))
    for spl_H in [modelspillH]: sHobject = getattr(instance, str(spl_H))
    for trf_L in [modelline]: linobject = getattr(instance, str(trf_L))
                
    for i in range(len(thermalPlants)):
        for j in lenblk:
            genThermal[k][s][i][j]=Tobject[thermalPlants[i],j+1].value
    
    for i in range(len(smallPlants)):
        for j in lenblk:
            genSmall[k][s][i][j]=Sobject[smallPlants[i],j+1].value
    
    for i in range(len(hydroPlants)):
        for j in lenblk:
            genHydro[k][s][i][j]=Hobject[hydroPlants[i],j+1].value
    
    for i in range(len(hydroPlants)):
        for j in lenblk:
            spillHydro[k][s][i][j]=sHobject[hydroPlants[i],j+1].value
            
    for i in range(numAreas):
        for j in lenblk:
            genwind[k][s][i][j]=Wobject[i+1,j+1].value
    
    for i in range(numAreas):
        for j in lenblk:
            spillwind[k][s][i][j]=sWobject[i+1,j+1].value
            
    for i in range(len(batteries)):
        for j in lenblk:
            genBatteries[k][s][i][j]=Bobject[batteries[i],j+1].value
    
    if numAreas is not 1:
        for i in range(len(linesData)):
            org = linesData[i][0]; dest = linesData[i][1]
            for j in lenblk:
                linTransfer[k][s][org-1][dest-1].append(linobject[org,dest,j+1].value - linobject[dest,org,j+1].value)
        
    for i in range(len(batteries)):
        for j in lenblk:
            loadBatteries[k][s][i][j]=cBobject[batteries[i],j+1].value
    
    for i in range(len(batteries)):
        lvlBatteries[k][s][i]=lBobject[batteries[i]].value
    
    for i in range(len(hydroPlants)):
        lvlHydro[k][s][i]=lHobject[hydroPlants[i]].value
    
    for i in range(numAreas):
        for j in lenblk:
            genDeficit[i][k][s][j]=Dobject[i+1,j+1].value
    
    return (genThermal,genHydro,genBatteries,genDeficit,loadBatteries,lvlBatteries,
           lvlHydro,linTransfer,spillHydro,genwind,spillwind,genSmall)


def printresults(dict_data,scenarios,stages,sol_scn):
    
    # load results
    from statistics import mean
    import pickle
    dict_results = pickle.load( open( "savedata/results_save.p", "rb" ) )
    dict_hydro = pickle.load( open( "savedata/hydro_save.p", "rb" ) )
    dict_format = pickle.load( open( "savedata/format_save.p", "rb" ) )
    dict_lines = pickle.load( open( "savedata/lines_save.p", "rb" ) )
    dict_sim = pickle.load( open( "savedata/format_sim_save.p", "rb" ) )
    
#    dict_windenergy = pickle.load( open( "savedata/windspeed_save.p", "rb" ) )
    
    # dictionary
    genThermal = dict_results["genThermal"]
    genSmall = dict_results["genSmall"]
    genHydro = dict_results["genHydro"]
    genBatteries = dict_results["genBatteries"]
    genDeficit = dict_results["genDeficit"]
    loadBatteries = dict_results["loadBatteries"]
    lvlBatteries = dict_results["lvlBatteries"]
    lvlHydro = dict_results["lvlHydro"]
    linTransfer = dict_results["linTransfer"]
    spillHydro = dict_results["spillHydro"]
    genwind = dict_results["genwind"]
    marg_costs = dict_results["marg_costs"]
    l_limits = dict_lines["l_limits"]
    
    batteries = dict_data['batteries']
    hydroPlants = dict_data['hydroPlants']
    thermalPlants = dict_data['thermalPlants']
    smallPlants = dict_data['smallPlants']
    numBlocks = dict_format['numBlocks']
    numAreas = dict_data['numAreas']
    battData = dict_data['battData']
    windPlants = dict_data['windPlants']
    volData = dict_data['volData']
    prodFactor = dict_hydro['prodFactor']
    thermalData = dict_data['thermalData']
    smallData = dict_data['smallData']
    linesData = dict_data['linesData']
#    costData = dict_data['costData']
    areasData = dict_data['areasData']
    yearvector = dict_sim['yearvector']
    
    #lenblk = range(numBlocks); lenthm = range(len(thermalPlants))
    lenstg = range(stages); lensc = range(scenarios) 
#    lenareas = range(numAreas)
    
    from openpyxl import Workbook
    
    # Create file
    wb = Workbook()
    dest_filename = 'results/Results.xlsx'
    
    ws0 = wb.active
    ws0.title = "Summary"
    
    ws0['A2'] = 'Scenarios'; ws0['B2'] = scenarios
    ws0['A3'] = 'Stages'; ws0['B3'] = stages
    ws0['A4'] = 'Blocks'; ws0['B4'] = numBlocks
    ws0['A5'] = 'Areas'; ws0['B5'] = numAreas
    ws0['A6'] = 'Hydro plants'; ws0['B6'] = len(hydroPlants)
    ws0['A7'] = 'Thermal plants'; ws0['B7'] = len(thermalPlants)
    ws0['A8'] = 'Wind plants'; ws0['B8'] = len(windPlants)
    ws0['A9'] = 'Batteries'; ws0['B9'] = len(batteries)
    ws0['A10'] = 'LinksAreas'; ws0['B10'] = len(linesData)
    
    ws1 = wb.create_sheet(title="HydroGen")
    
    ###########################################################################
    
#    # determine marginal cost for hydro plants
    plants = len(hydroPlants)
#    selection = []
#    for k in lenareas:
#        indexplants = [i for i,val in enumerate(volData[11]) if val==k+1] # index plant at each area
#        plantsarea = [hydroPlants[i] for i in indexplants]
#        costarea = [volData[8] for i in indexplants]
#        print(costarea)
#        actual = costarea.index(min(costarea))
##        aux = costarea[0]
##        for z in range(len(costarea)):
##            if costarea[z] <= aux:
##                selplant = plantsarea[z]
##                aux = costarea[z]
#        selection.append(plantsarea[actual])
#    marginalhydro = [[[ [selection[z]]*numBlocks for z in lenareas ] for x in lenstg] for y in lensc]  
    
    genHFinal = [[[[] for y in range(scenarios) ] for z in range(plants) ] for x in range(stages)]
    
    plants = len(hydroPlants); 
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
                # amount of energy produced  
                valuegen = 0
                
                for x in range(numBlocks):
                    valuegen += genHydro[i][j][k][x]*prodFactor[k][j]
                    
#                    # marginal costs
#                    areagen = int(volData[11][k])
#                    genplant = genHydro[i][j][k][x]
#                    if genplant > 0:
#                        actual = hydroPlants.index(marginalhydro[i][j][areagen-1][x])
#                        if volData[8][k] > volData[8][actual]:
#                            marginalhydro[i][j][areagen-1][x] = hydroPlants[k]
#                        else:
#                            pass
                        
                    _ = ws1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=genHydro[i][j][k][x]*prodFactor[k][j])
                    _ = ws1.cell(column=2, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws1.cell(column=1, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=2, value=hydroPlants[k])
                    _ = ws1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4, value='Scenario:'+str(i+1) )
                    _ = ws1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3, value='Area:'+str(volData[11][k]) )
                genHFinal[j][k][i] = valuegen
    ws1['B4'] = 'block'; ws1['A4'] = 'Stage'   
    
    # spill of water
    ws1_1 = wb.create_sheet(title="SpillHydro")
    
    plants = len(hydroPlants)
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
                for x in range(numBlocks):
                    _ = ws1_1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), 
                                 value=spillHydro[i][j][k][x]/(yearvector[j]*3600*1e-6))
                    
                    _ = ws1_1.cell(column=2, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws1_1.cell(column=1, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws1_1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=2, value=hydroPlants[k])
                    _ = ws1_1.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3, value='Scenario:'+str(i+1) )
    ws1_1['B3'] = 'block'; ws1_1['A3'] = 'Stage'; ws1_1['A1'] = 'spill m3/s ' 
    
    # level of reservoirs at the end of each stage
    levelhydroScn = [[[[] for y in range(plants) ] for z in range(scenarios) ] for x in range(stages)]
    
    ws10 = wb.create_sheet(title="LevelHydro")
    
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
               
                levelhydroScn[j][i][k] = lvlHydro[i][j][k]
               
                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=4+(j+1), value=lvlHydro[i][j][k])
                _ = ws10.cell(column=1, 
                             row=4+(j+1), value=j+1)
                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=2, value=hydroPlants[k])
                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=3, value= 'Area:'+str(volData[11][k]))
                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4, value='Scenario:'+str(i+1) )
    ws10['A4'] = 'Stage'
    
    # Aggregated reservoir
    Aggrhydro = [[[] for z in range(3) ] for x in range(stages)]
    for j in range(stages):
        sumpl = []
        for i in range(scenarios):
            sumpl.append(sum(levelhydroScn[j][i]))
        
        Aggrhydro[j][1] = mean(sumpl) 
        Aggrhydro[j][0] = min(sumpl); Aggrhydro[j][2] = max(sumpl)
    
    ws10_2 = wb.create_sheet(title="AggLevelHydro")
    
    for j in range(stages):
        for k in range(3):
            
            _ = ws10_2.cell(column=1+(k+1),row=2+(j+1), value=Aggrhydro[j][k])
            _ = ws10.cell(column=1, row=2+(j+1), value=j+1)
            _ = ws10_2.cell(column=2,row=2, value='min[Hm3]' )
            _ = ws10_2.cell(column=3,row=2, value='aveg[Hm3]' )
            _ = ws10_2.cell(column=4,row=2, value='max[Hm3]' )
    ws10_2['A2'] = 'Stage'
    
    ###########################################################################
    
    # Thermal plants analysis
    
    ws2 = wb.create_sheet(title="ThermalGen")
    
    plants = len(thermalPlants)
#    selection = []
#    for k in lenareas:
#        indexplants = [i for i,val in enumerate(thermalData[3]) if val==k+1]
#        plantsarea = [thermalPlants[i] for i in indexplants]
#        costarea = [costData[i][0] for i in indexplants]
#        actual = costarea.index(min(costarea))
##        aux = costarea[0]
##        for z in range(len(costarea)):
##            if costarea[z] <= aux:
##                selplant = plantsarea[z]
##                aux = costarea[z]
#        selection.append(plantsarea[actual])
#    marginal = [[[ [selection[z]]*numBlocks for z in lenareas ] for x in lenstg] for y in lensc]    
    
    genTFinal = [[[[] for y in range(scenarios) ] for z in range(plants) ] for x in range(stages)]
                
    # check if the selected plant in list is actually generating / otherwise cost is zero
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
               # amount of energy produced 
               valuegen = 0
               for x in range(numBlocks):
                   
                   valuegen += genThermal[i][j][k][x]
#                   # marginal costs
#                   areagen = int(thermalData[3][k])
#                   genplant = genThermal[i][j][k][x]
#                   if genplant > 0:
#                       actual = thermalPlants.index(marginal[i][j][areagen-1][x])
#                       if costData[k][j] > costData[actual][j]:
#                           marginal[i][j][areagen-1][x] = thermalPlants[k]
#                       else:
#                           pass
                            
                   # write results
                   _ = ws2.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=genThermal[i][j][k][x])
                   _ = ws2.cell(column=2, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                   _ = ws2.cell(column=1, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                   _ = ws2.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=2, value=thermalPlants[k])
                   _ = ws2.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=4, value='Scenario:'+str(i+1) )
                   _ = ws2.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=3, value='Area:'+str(int(thermalData[3][k])) )
               genTFinal[j][k][i] = valuegen
               
    ws2['B4'] = 'block'; ws2['A4'] = 'Stage'   
    
        ###########################################################################
    
    # Small plants analysis
    
    ws22 = wb.create_sheet(title="SmallGen")
    
    plants = len(smallPlants)
    genSFinal = [[[[] for y in range(scenarios) ] for z in range(plants) ] for x in range(stages)]
                
    # check if the selected plant in list is actually generating / otherwise cost is zero
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
               # amount of energy produced 
               valuegen = 0
               for x in range(numBlocks):
                   
                   valuegen += genSmall[i][j][k][x]

                   # write results
                   _ = ws22.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=genSmall[i][j][k][x])
                   _ = ws22.cell(column=2, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                   _ = ws22.cell(column=1, 
                                row=4+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                   _ = ws22.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=2, value=smallPlants[k])
                   _ = ws22.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=4, value='Scenario:'+str(i+1) )
                   _ = ws22.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                row=3, value='Area:'+str(int(smallData[3][k])) )
               genSFinal[j][k][i] = valuegen
               
    ws22['B4'] = 'block'; ws22['A4'] = 'Stage'  
    
    ###########################################################################
    
    # Marginal cost           
    ws21 = wb.create_sheet(title="MarginalCost")
    genMFinal = [[[[] for z in range(scenarios)] for z in range(numAreas) ] for x in range(stages)]
    
    for k in range(scenarios):
        for i in range(numAreas):
            _ = ws21.cell(column=1+(k+1)+(i+1)*scenarios-scenarios,row=3, value='Scenario:'+str(k+1) )
            _ = ws21.cell(column=1+(k+1)+(i+1)*scenarios-scenarios,row=2, value='Area:'+ areasData[0][i] )
            for j in range(stages):
                valueC = 0
                for x in range(numBlocks):
                    valueC2 = sum(marg_costs[k][j][i][x])
                    if valueC2 > valueC:
                        valueC = valueC2
                    
                genMFinal[j][i][k] = valueC #/numBlocks
                
                _ = ws21.cell(column=1+(k+1)+(i+1)*scenarios-scenarios,row=3+(j+1), value=valueC)
                _ = ws21.cell(column=1, row=3+(j+1), value=j+1)
                
    ws21['A3'] = 'Stage'
    
    ws21_2 = wb.create_sheet(title="MarginalArea")
    
    for k in range(numAreas):
        _ = ws21_2.cell(column=1+(k+1),row=2, value='Area:'+ areasData[0][k] )
        for j in range(stages):
            _ = ws21_2.cell(column=1+(k+1), row=2+(j+1), value=sum(genMFinal[j][k])/scenarios)
            _ = ws21_2.cell(column=1, row=2+(j+1), value=j+1)
            
    ws21_2['A2'] = 'Stage'

    ###########################################################################

    # Wind generation by areas    
    ws3 = wb.create_sheet(title="WindGen")
    
    genWFinal = [[[[] for y in range(scenarios) ] for z in range(numAreas) ] for x in range(stages)]
    
    for i in range(scenarios):
        for j in range(stages):
           for k in range(numAreas):
                valueW = 0
                for x in range(numBlocks):
                    
                    valueW += genwind[i][j][k][x]
                    
                    _ = ws3.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), 
                                 value=genwind[i][j][k][x])
                    _ = ws3.cell(column=2, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws3.cell(column=1, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws3.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=2, value='Area: '+areasData[0][k])
                    _ = ws3.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3, value='Scenario:'+str(i+1) )
                genWFinal[j][k][i] = valueW
                
    ws3['B3'] = 'block'; ws3['A3'] = 'Stage'  
    
    ###########################################################################
    
    # Batteries generation
    ws4 = wb.create_sheet(title="BatteriesGen")
    
    plants = len(batteries)
    genBFinal = [[[[] for y in range(scenarios) ] for z in range(plants) ] for x in range(stages)]
    
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
                valueB = 0
                for x in range(numBlocks):
                    valueB += genBatteries[i][j][k][x]
                    
                    _ = ws4.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=genBatteries[i][j][k][x])
                    _ = ws4.cell(column=2, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws4.cell(column=1, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws4.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=2, value=batteries[k])
                    _ = ws4.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3, value='Area:'+str(int(battData[9][k])) )
                    _ = ws4.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4, value='Scenario:'+str(i+1) )
                genBFinal[j][k][i] = valueB
                    
    ws4['B4'] = 'block'; ws4['A4'] = 'Stage'  
    
    ###########################################################################
    
    ws5 = wb.create_sheet(title="Deficit")
    
    genDFinal = [[[[] for y in range(scenarios) ] for z in range(numAreas) ] for x in range(stages)]
    
    for k in range(numAreas):
        for i in range(scenarios):
            for j in range(stages):
                valueD = 0
                
                for x in range(numBlocks):
                    valueD += genDeficit[k][i][j][x]
                    
                    _ = ws5.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), 
                                 value=genDeficit[k][i][j][x])
                    _ = ws5.cell(column=2, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws5.cell(column=1, 
                                 row=3+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws5.cell(column=2+(i+1)+(k+1)*scenarios-scenarios,
                                 row=3, value='Scenario:'+str(i+1) )
                    _ = ws5.cell(column=2+(i+1)+(k+1)*scenarios-scenarios,
                                 row=2, value='Area:'+str(k+1) )
                genDFinal[j][k][i] = valueD
                
    ws5['B3'] = 'block'; ws5['A3'] = 'Stage' 
    
    ###########################################################################
    
#    ws6 = wb.create_sheet(title="SpillWind")
    
#    plants = len(windPlants); 
#    for i in range(scenarios):
#        for j in range(stages):
#           for k in range(plants):
#                for x in range(blk):
#                    _ = ws6.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
#                                 row=3+(j+1)*blk-blk+(x+1), value=spillWind[i][j][k][x])
#                    _ = ws6.cell(column=2, 
#                                 row=3+(j+1)*blk-blk+(x+1), value=x+1)
#                    _ = ws6.cell(column=1, 
#                                 row=3+(j+1)*blk-blk+(x+1), value=j+1)
#                    _ = ws6.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
#                                 row=2, value=windPlants[k])
#                    _ = ws6.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
#                                 row=3, value='Scenario:'+str(i+1) )
#    ws6['B3'] = 'block'; ws6['A3'] = 'Stage' 
    
    ###########################################################################
    
    ws8 = wb.create_sheet(title="LevelBatt")
    
    plants = len(batteries);
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
                _ = ws8.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=4+(j+1), value=lvlBatteries[i][j][k])
                _ = ws8.cell(column=1, 
                             row=4+(j+1), value=j+1)
                _ = ws8.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=2, value=batteries[k])
                _ = ws8.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                             row=3, value='Area:'+str(int(battData[9][k])) )
                _ = ws8.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4, value='Scenario:'+str(i+1) )
    ws8['A4'] = 'Stage'
    
    ws7 = wb.create_sheet(title="LoadBatt")
    
    plants = len(batteries);
    for i in range(scenarios):
        for j in range(stages):
           for k in range(plants):
                for x in range(numBlocks):
                    _ = ws7.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=loadBatteries[i][j][k][x])
                    _ = ws7.cell(column=2, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=x+1)
                    _ = ws7.cell(column=1, 
                                 row=4+(j+1)*numBlocks-numBlocks+(x+1), value=j+1)
                    _ = ws7.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=2, value=batteries[k])
                    _ = ws7.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=3, value='Area:'+str(int(battData[9][k])) )
                    _ = ws7.cell(column=2+(i+1)+(k+1)*scenarios-scenarios, 
                                 row=4, value='Scenario:'+str(i+1) )
    ws7['B4'] = 'block'; ws7['A4'] = 'Stage' 
    
    ###########################################################################
    if numAreas is not 1:
        
        scnTransfer = [[[[[] for y in lensc] for y in lenstg] for a in range(numAreas)] for z in range(numAreas)] 
        
        ws10 = wb.create_sheet(title="EnergyTransfer")
        lineval = int(len(linesData))
        for i in range(scenarios):
            _ = ws10.cell(column=4+(i+1),row=2, value='Scenario:'+str(i+1) )
            for j in range(stages):
                for k in range(lineval):
                    org = linesData[k][0]; dest = linesData[k][1]
                    for x in range(numBlocks):
                        
                        scnTransfer[org-1][dest-1][j][i].append(linTransfer[i][j][org-1][dest-1][x])
                        
                        _ = ws10.cell(column=4+(i+1), 
                                     row=2+((j+1)*numBlocks-numBlocks+(x+1))*lineval-lineval+(k+1), value=linTransfer[i][j][org-1][dest-1][x])
                        _ = ws10.cell(column=2, 
                                     row=2+((j+1)*numBlocks-numBlocks+(x+1))*lineval-lineval+(k+1), value=x+1)
                        _ = ws10.cell(column=1, 
                                     row=2+((j+1)*numBlocks-numBlocks+(x+1))*lineval-lineval+(k+1), value=j+1)
                        _ = ws10.cell(column=3, 
                                     row=2+((j+1)*numBlocks-numBlocks+(x+1))*lineval-lineval+(k+1), value=org)
                        _ = ws10.cell(column=4, 
                                     row=2+((j+1)*numBlocks-numBlocks+(x+1))*lineval-lineval+(k+1), value=dest)
                        
                    
        ws10['B2'] = 'block'; ws10['A2'] = 'Stage'; ws10['C2'] = 'Area:From'; ws10['D2'] = 'Area:To'  
        
        ws10_1 = wb.create_sheet(title="EnergyTransferStage")
        for k in range(lineval):
            org = linesData[k][0]; dest = linesData[k][1]
            _ = ws10_1.cell(column=1+(k+1), row=2, value='from:'+str(areasData[0][org-1])+' to:'+str(areasData[0][dest-1]))
            for j in range(stages):
                trf_stg = 0
                for i in range(scenarios):
                    trf_stg += sum(scnTransfer[org-1][dest-1][j][i])#-sum(scnTransfer[dest-1][org-1][j][i])
                    
                _ = ws10_1.cell(column=1+(k+1), row=2+(j+1), value = trf_stg/scenarios)
                _ = ws10_1.cell(column=1, row=2+(j+1), value=j+1)
        
        ws10_1['A2'] = 'stage'
        
        ws10_2 = wb.create_sheet(title="Transferlimits")
        for k in range(lineval):
            org = linesData[k][0]; dest = linesData[k][1]
            _ = ws10_2.cell(column=1+(k+1), row=2, value='from:'+str(areasData[0][org-1])+' to:'+str(areasData[0][dest-1]))
            for j in range(stages):
                    
                _ = ws10_2.cell(column=1+(k+1), row=2+(j+1), value = l_limits[j][org-1][dest-1])
                _ = ws10_2.cell(column=1, row=2+(j+1), value=j+1)
        
        ws10_2['A2'] = 'stage'

#        ws10_3 = wb.create_sheet(title="TransferlimitsD")
#        for k in range(lineval):
#            org = linesData[k][0]; dest = linesData[k][1]
#            _ = ws10_3.cell(column=1+(k+1), row=2, value='from:'+str(areasData[0][org-1])+' to:'+str(areasData[0][dest-1]))
#            for j in range(stages):
#                    
#                _ = ws10_3.cell(column=1+(k+1), row=2+(j+1), value = l_limits[j][dest-1][org-1])
#                _ = ws10_3.cell(column=1, row=2+(j+1), value=j+1)
#        
#        ws10_3['A2'] = 'stage'         
    
    ###########################################################################
    
#    ws9 = wb.create_sheet(title="OperativeCost")
#
#    for i in range(scenarios):
#        for j in range(stages):
#                _ = ws9.cell(column=1+(i+1), 
#                             row=2+(j+1), value=sol_scn[i][j])
#                _ = ws9.cell(column=1, 
#                             row=2+(j+1), value=j+1)
#                _ = ws9.cell(column=1+(i+1), 
#                                 row=2, value='Scenario:'+str(i+1) )
#    ws9['A2'] = 'Stage'

    ###########################################################################
    
    # create graphics
    
#    ws10 = wb.create_sheet(title="Total")
#    
#    plants = len(hydroPlants); 
#    for i in range(scenarios):
#        for j in range(stages):
#           for k in range(plants):
#               
#                valuegen = 0
#                for x in range(numBlocks):
#                    valuegen += genHydro[i][j][k][x]*prodFactor[k][j]
#                    
#                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=4+(j+1), value=valuegen)
#                _ = ws10.cell(column=1, 
#                             row=4+(j+1), value=j+1)
#                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=2, value=hydroPlants[k])
#                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=4, value='Scenario:'+str(i+1) )
#                _ = ws10.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=3, value='Area:'+str(volData[11][k]) )
#    ws10['A4'] = 'Stage'  
#    
#    ws11 = wb.create_sheet(title="Total2")
#    plants = len(thermalPlants); 
#    for i in range(scenarios):
#        for j in range(stages):
#           for k in range(plants):
#                
#                valuegen = 0
#                for x in range(numBlocks):
#                    valuegen += genThermal[i][j][k][x]
#                    
#                # write results
#                _ = ws11.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=4+(j+1), value=valuegen)
#                _ = ws11.cell(column=1, row=4+(j+1), value=j+1)
#                _ = ws11.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=2, value=thermalPlants[k])
#                _ = ws11.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=4, value='Scenario:'+str(i+1) )
#                _ = ws11.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                             row=3, value='Area:'+str(int(thermalData[3][k])) )
#    ws11['A4'] = 'Stage' 
    
#    ws12 = wb.create_sheet(title="Total3")
#    
#    for k in range(numAreas):
#        for i in range(scenarios):
#            for j in range(stages):
#                
#                valuedef = 0
#                for x in range(numBlocks):
#                    valuedef += genDeficit[k][i][j][x]
#                    
#                    _ = ws12.cell(column=1+(i+1)+(k+1)*scenarios-scenarios, 
#                                 row=3+(j+1), value=valuedef)
#                    _ = ws12.cell(column=1, 
#                                 row=3+(j+1), value=j+1)
#                    _ = ws12.cell(column=1+(i+1)+(k+1)*scenarios-scenarios,
#                                 row=3, value='Scenario:'+str(i+1) )
#                    _ = ws12.cell(column=1+(i+1)+(k+1)*scenarios-scenarios,
#                                 row=2, value='Area:'+str(k+1) )
#                
#    ws5['A3'] = 'Stage' 

                    
    wb.save(filename = dest_filename)
    
    ###########################################################################
    
    datasol = {"genHFinal":genHFinal,"genTFinal":genTFinal,"genWFinal":genWFinal,
               "genDFinal":genDFinal,"genBFinal":genBFinal,"genMFinal":genMFinal,
               "genSFinal":genSFinal}
    
    pickle.dump(datasol, open( "savedata/html_save.p", "wb" ) )
            
    # return marginal

def pyomoformat(set1, set2, param):
    
    """
    Function that flattens the multidimensional dispaset input data into the pyomo format:
    a dictionnary with a tuple and the parameter value.
    The tuple contains the strings of the corresponding set values
    """

    param_new = {}; data = 0
    for k in range(len(set1)):
        varset1 = set1[k]
        for z in range(len(set2)):
            varset2 = set2[z]
            param_new[(varset1, varset2)] = param[data]
            data = data + 1

    return param_new

###############################################################################
    
def inflowswind(stages,scenarios,wind_quantile,wind_var,dict_data,dict_format):
    
    windData = dict_data['windData']
    yearvector = dict_format['yearvector']
    windPlants = dict_data['windPlants']
    blocksData = dict_data['blocksData']
    numAreas = dict_data['numAreas']
    
    inflowsWind = [[[] for x in range(scenarios) ] for y in range(stages)]
    for i in range(stages, 0, -1):
        for k in range(scenarios):
            
            for area in range(numAreas):
                
                # Set of plants for each area
                windP = 0; plantsArea = []
                for z in range(len(windPlants)):
                    if windData[8][z] == area+1:
                        windP += 1
                        plantsArea.append(windPlants[z])
                        
                writer1 = []
                for j in range(len(blocksData[0])): 
                    win_m = 0; win_var = 0
                    cdf = wind_quantile[area][i-1][k][j]
        
                    for m in range(windP):
                        # data wind : mean and variance
                        block_speed = wind_var[area][m][i-1][k][j]  
                        
                        idx = dict_data['windPlants'].index(plantsArea[m])
                        
                        # mean                
                        win_gen_m = block_speed[0]**3 + 3*block_speed[0]*block_speed[1]
                        win_gen_m = (3.14/8)*windData[2][idx]*windData[3][idx]*(windData[4][idx]**2)*windData[5][idx]*win_gen_m          
                        win_m += win_gen_m*yearvector[i-1]*blocksData[0][j]/1000
                        
                        # variance
                        win_var_m = 3*block_speed[1]*( (3*block_speed[0]**4) + (12*block_speed[0]**2*block_speed[1]) + (5*block_speed[1]**2) )
                        win_var += (((3.14/8)*windData[2][idx]*windData[3][idx]*(windData[4][idx]**2)*windData[5][idx])**2)*win_var_m
                    
                    if cdf == 9999:
                        writer1.append(0)
                    else:
                        energy = win_m + (cdf*win_var**0.5*yearvector[i-1]*blocksData[0][j]/1000)
                        if energy < 0:
                            writer1.append(0)
                        else:    
                            writer1.append(win_m + (cdf*win_var**0.5*yearvector[i-1]*blocksData[0][j]/1000) )       
                        
                # Save area production
                inflowsWind[i-1][k].append(writer1)
            
    return inflowsWind

###############################################################################

def cutsback(stage,dict_data,dict_format,dict_hydro,sol_vol,iteration,
             sol_lvl,dict_batt):

    hydroPlants = dict_data['hydroPlants']
    batteries = dict_data['batteries']
    b_storage = dict_batt['b_storage']
    battData = dict_data['battData']
    volData = dict_data['volData']
    
    # calculate the volume states
    if stage == 1:
        # initial conditiona
        cuts_reservoirs = []
        for m in range(len(hydroPlants)): # Loop for all hydro plants
            vol_cut = [volData[0][m]]
            cuts_reservoirs.append(vol_cut)
    else:
        # empty reservoirs
        cuts_reservoirs = []
        for m in range(len(hydroPlants)): # Loop for all hydro plants
            vol_cut = [volData[1][m]]
            cuts_reservoirs.append(vol_cut)
        
    # calculate the storage cuts
    cuts_storage = []
    for m in range(len(batteries)): # Loop for all batteries
        lvl_cut = [b_storage[m][stage-1][1]*battData[1][m]]
        cuts_storage.append(lvl_cut) 

    if iteration ==0:
            
        # Hydro plants
        cuts_iter_stage = []
        cuts_iter_stage.extend(cuts_reservoirs)
        # Batteries
        cuts_iter_stage_B = []
        cuts_iter_stage_B.extend(cuts_storage)
        
    else:
        
        # HYDRO - Results from last iteration
        cuts_iter_stage = [[] for x in range(len(hydroPlants))]
        # Loop for include vol_fin from the last iteration
        for c in range(len(hydroPlants)): 
            cuts_iter_stage[c] = list(cuts_reservoirs[c])
        for c in range(len(hydroPlants)): 
            for k in range(len(sol_vol[stage-1])):
                cuts_iter_stage[c].append(sol_vol[stage-1][k][c])
                # aux_vec = cuts_iter_stage[c]; print(aux_vec)
        
            
        cuts_iter_stage_B = [[] for x in range(len(batteries))]
        # Loop for include lvl_fin from the last iteration
        for c in range(len(batteries)): 
            cuts_iter_stage_B[c] = list(cuts_storage[c])
        for c in range(len(batteries)):
            for k in range(len(sol_lvl[stage-1])):
                cuts_iter_stage_B[c].append(sol_lvl[stage-1][k][c])
                    
    return cuts_iter_stage,cuts_iter_stage_B

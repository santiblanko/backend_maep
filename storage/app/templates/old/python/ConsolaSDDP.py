''' Version 2.0 MPODE Simplificado '''

# Simulation time
import timeit
start = timeit.default_timer()
import sys, json

#==============================================================================
# read options SI or NO
read_data = 'NO'
if read_data is "NO": 

    #Reading system information
    import readData
    readData.data('datasystem/prueba.xlsx')
    
read_wind_data = 'NO'
if read_wind_data is "NO": 
    
    from utils.readWind import historical10m, format10m
    windalldata = historical10m()
    windmat = format10m()

#==============================================================================
# Parameters simulation
max_iter = 1 # Maximun number of iterations
extra_stages = 2
stages = 3 + extra_stages # planning horizon: stagesData (10 years + 2 years no analysis)
seriesBack = 2 # series used in the backward phase
seriesForw = 2 # series used in the forward phase
# deterministic / stochastic analysis
stochastic = 1
if seriesBack == 1: stochastic = 0 # Deterministic
 
# Parameters analysis
variance = 0.01 # montecarlo chort term analysis - wind variation
sensDem = 1.0 # Demand factor
speed_out = 17 # Turbines
speed_in = 4.5 # Turbines
eps_area = 0.5 # significance level area
eps_all = 0.8 # significance level for the whole system
eps_risk = 0.1 # long-term risk
commit = 0.0 # risk-measure comminment

# export options SI or NO
print_results = 'SI'; export = 0
if print_results is "SI": 
    export = 1
    import reports

# Load the dictionary back from the pickle file.
import pickle
dict_data = pickle.load( open( "savedata/data_save.p", "rb" ) )

#==============================================================================
# calculation options SI or NO
parameters_calculation = 'SI'
if parameters_calculation is "SI": 
    
    # Creating fixed input files '''
    from utils.input_data import inputdata
    dict_format = inputdata(dict_data,sensDem)    
    
    from utils.input_hydro import inputhydro
    dic_hydro = inputhydro(dict_data,dict_format) 
    
    from utils.input_wind import inputwind, inputInflowWind
    dict_wind = inputwind(dict_data,dict_format,stages)
    inputInflowWind(dict_data,dict_format,dict_wind,stages,variance,
    speed_out,speed_in,eps_area,eps_all)
    
    from utils.input_others import inputbatteries, inputlines
    inputbatteries(dict_data,dict_format,stages)
    inputlines(dict_data,dict_format,stages)

#==============================================================================
# Optimization section
                                                 
# dictionaries
batteries = dict_data['batteries']
hydroPlants = dict_data['hydroPlants']
dict_batt = pickle.load( open( "savedata/batt_save.p", "rb" ) )

# Iteration inf _ improve the states under evaluation
sol_vol = [[] for x in range(stages+1)] # Hydro iteration
sol_lvl = [[] for x in range(stages+1)] # Batteries iteration
sol_vol[0].append(dict_data['volData'][0])
sol_lvl[0].append([dict_data['battData'][0][x]*dict_batt["b_storage"][x][0][0] for x in range(len(batteries))])
    
iteration = 0 # Counter for number of iterations
confidence = 0 # stop criteria

# Save operational cost by iteration
operative_cost = [[],[]] 

# simulation parameters
parallel_calculation = 'NO' # parallel options SI or NO
policy = "SI"; pol = 0
if policy is "SI": pol = 1
simulation = "SI"; sim = 0
if simulation is "SI": sim = 1
    
###############################################################################

# loop iterations
import forward, backward, optimality

if pol == 1 and sim == 1:
    
    while not iteration >= max_iter and not confidence >= 2:
        
        # save the FCF - backward steps
        fcf_backward = [[] for x in range(stages+1)]
        fcf_backward[stages]=[[[0]*len(hydroPlants),[0]*len(batteries),0]]
        
        # Backward_Risk6_par to parallel simulation
        fcf_backward, sol_vol = backward.data(stages,seriesBack,stochastic,
        fcf_backward,sol_vol,iteration,sol_lvl,eps_risk,commit,parallel_calculation)
        
        # Forward stage - Pyomo module  
        (sol_vol,sol_lvl,sol_costs,sol_scn) = forward.data(confidence,fcf_backward,
        stages,seriesForw,iteration,sol_vol,sol_lvl,max_iter,export)
            
        # confidence          
        confd, op_cost, op_inf = optimality.data(sol_costs,seriesForw)
        confidence += confd; iteration += 1
    
        # Saver results
        operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)
        
        if iteration + 1 == max_iter or confidence == 2:
            datafcf = {"fcf_backward":fcf_backward,"sol_vol":sol_vol,"sol_lvl":sol_lvl,'sol_scn':sol_scn}
            pickle.dump(datafcf, open( "savedata/fcfdata.p", "wb" ) )

elif pol == 0 and sim == 1:
    
    # stages to be simulate
    dict_fcf = pickle.load( open( "savedata/fcfdata.p", "rb" ) )
    fcf_backward = dict_fcf['fcf_backward']
    
    sol_voll = dict_fcf['sol_vol']
    sol_lvl = dict_fcf['sol_lvl']
    iteration = 0; confidence = 1
    
    # Forward stage - Pyomo module  
    (sol_vol,sol_lvl,sol_costs,sol_scn) = forward.data(confidence,fcf_backward,
    stages,seriesForw,iteration,sol_vol,sol_lvl,max_iter,export)
    
    # Saver results
    confidence, op_cost, op_inf = optimality.data(sol_costs,seriesForw)
    operative_cost[0].append(op_cost), operative_cost[1].append(op_inf)
    
if export == 1:
    
#    if sim == 0:
#        dict_fcf = pickle.load( open( "savedata/fcfdata.p", "rb" ) )
#        sol_scn = dict_fcf['sol_scn']
    
    from utils.saveresults import printresults
    marginal = printresults(dict_data,seriesForw,(stages-extra_stages),sol_scn)
    
    # generate html report
    reports.data(seriesForw,(stages-extra_stages))
        
# print execution time
end = timeit.default_timer()
print(end - start)


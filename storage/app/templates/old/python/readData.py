def data(case):
    
    import openpyxl
    import csv
    import datetime
    import pickle
   
    from utils.readxlxs import xlxstocsv
    
    importedfile = openpyxl.load_workbook(filename = case, read_only = True, keep_vba = False)
    tabnames = importedfile.get_sheet_names()
    
    ###########################################################################
    substring = "Areas"
    xlxstocsv(tabnames,substring,importedfile)
    
    with open('datasystem/Areas.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        areasData = [[] for x in range(2)];  
        for row in readCSV:
            val = row[0]
            try: 
                val = int(row[0])
            except ValueError:
                pass
            areasData[1].append(val)
            areasData[0].append(row[1])
    for col in range(2):
        areasData[col].pop(0) 
        
    numAreas = len(areasData[0]) # Number of areas in the system
    
    ###########################################################################

    substring = "Demand"
    xlxstocsv(tabnames,substring,importedfile)
                 
    with open('datasystem/Demand.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break                
    with open('datasystem/Demand.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        demandData = [[] for x in range(columns-1)];  
        for row in readCSV:
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = float(row[col+1])
                except ValueError:
                    pass
                demandData[col].append(val)
    for col in range(columns-1):
        demandData[col].pop(0) 
    
    with open('datasystem/Demand.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        horizon = [];  
        for row in readCSV:
            val = row [0]
            try: 
                val = datetime.datetime.strptime (row [0],"%Y-%m-%d %H:%M:%S") 
            except ValueError:
                pass
            horizon.append(val)
    horizon.pop(0) 

    ###########################################################################
    
    substring = "ThermalConfiguration"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/ThermalConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/ThermalConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        thermalPlants = []; thermalData = [[] for x in range(columns-1)]  
        for row in readCSV:
            tplants = row[0]; 
            thermalPlants.append(tplants)
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = float(row[col+1])
                except ValueError:
                    pass
                thermalData[col].append(val)
        thermalPlants.pop(0)
    for col in range(columns-1):
        thermalData[col].pop(0)
    
    state = thermalData[1]
    for z in range(len(state)):
        if state[z] == "E":
            thermalData[1][z] = 1
        elif state[z] == "NE":
            thermalData[1][z] = 0
        else:
            val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
            location = horizon.index(val)
            thermalData[1][z] = location+1
    
    actives = [i for i, e in enumerate(thermalData[1]) if e != 0]
    thermalPlants_act = []; thermalData_act = [[] for x in range(columns-1)]
    for col in range(columns-1):
        for z in range(len(actives)):
            thermalData_act[col].append(thermalData[col][actives[z]])
    for z in range(len(actives)):
            thermalPlants_act.append(thermalPlants[actives[z]])
    
    nameAreas = thermalData_act[3]
    for z in range(len(nameAreas)):
        location = areasData[0].index(nameAreas[z])
        thermalData_act[3][z] = location+1
    
    ###########################################################################
    
    substring = "ThermalExpansion"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/ThermalExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/ThermalExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        expThData = [[] for x in range(columns)]  
        for row in readCSV:
            expThData[0].append(row[0])
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                expThData[col+1].append(val)
    for col in range(columns):
        expThData[col].pop(0)
    
    state = expThData[1]
    for z in range(len(state)):
        val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
        location = horizon.index(val)
        expThData[1][z] = location+1
            
    ###########################################################################
    
    substring = "OperationCost"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/ThermalConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        row_count = sum(1 for row in readCSV)                
    with open('datasystem/OperationCost.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        costData = [[] for x in range(row_count-1)]  
        for row in readCSV:
            for col in range(row_count-1):
                val = row[col+1]
                try: 
                    val = float(row[col+1])
                except ValueError:
                    pass
                costData[col].append(val)
    for col in range(row_count-1):
        costData[col].pop(0)
    
    costData_act = []
    for z in range(len(actives)):
        costData_act.append(costData[actives[z]])
    
    # OyM variable
    for z in range(len(actives)):
        costData_act[z] = [x + thermalData_act[10][z] for x in costData_act[z]]
        
    ###########################################################################
    
    substring = "RationingCosts"
    xlxstocsv(tabnames,substring,importedfile)
    
    with open('datasystem/RationingCosts.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break 
    with open('datasystem/RationingCosts.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        rationingData = [[] for x in range(columns-1)];  
        for row in readCSV:
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = float(row[col+1])
                except ValueError:
                    pass
                rationingData[col].append(val)
    for col in range(columns-1):
        rationingData[col].pop(0)    
    
    ###########################################################################

    substring = "Blocks"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/Blocks.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/Blocks.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        blocksData = [[] for x in range(columns-1)]  
        for row in readCSV:
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = float(row[col+1])
                except ValueError:
                    pass
                blocksData[col].append(val)
    for col in range(columns-1):
        blocksData[col].pop(0)
        
    ###########################################################################
    
    substring = "BatteriesConfiguration"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/BatteriesConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/BatteriesConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        batteries = []; battData = [[] for x in range(columns-1)]  
        for row in readCSV:
            tplants = row[0]; 
            batteries.append(tplants)
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                battData[col].append(val)
        batteries.pop(0)
    for col in range(columns-1):
        battData[col].pop(0)
    
    state = battData[6]
    for z in range(len(state)):
        if state[z] == "E":
            battData[6][z] = 1
        elif state[z] == "NE":
            battData[6][z] = 0
        else:
            val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
            location = horizon.index(val)
            battData[6][z] = location+1
    
    actives = [i for i, e in enumerate(battData[6]) if e != 0]
    batteries_act = []; battData_act = [[] for x in range(columns-1)]
    for col in range(columns-1):
        for z in range(len(actives)):
            battData_act[col].append(battData[col][actives[z]])
    for z in range(len(actives)):
            batteries_act.append(batteries[actives[z]])
    
    # Identify areas by codde        
    nameAreas = battData_act[9]
    for z in range(len(nameAreas)):
        location = areasData[0].index(nameAreas[z])
        battData_act[9][z] = location+1
        
    ###########################################################################    
    
    substring = "BattExpansion"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/BattExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/BattExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        expBttData = [[] for x in range(columns)]  
        for row in readCSV:
            expBttData[0].append(row[0])
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                expBttData[col+1].append(val)
    for col in range(columns):
        expBttData[col].pop(0)
    
    state = expBttData[1]
    for z in range(len(state)):
        val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
        location = horizon.index(val)
        expBttData[1][z] = location+1
            
    ###########################################################################
    
    substring = "HydroConfiguration"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/HydroConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/HydroConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        hydroPlants = []; volData = [[] for x in range(columns-1)]  
        for row in readCSV:
            tplants = row[0]; 
            hydroPlants.append(tplants)
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                volData[col].append(val)
        hydroPlants.pop(0)
    for col in range(columns-1):
        volData[col].pop(0)
    
    state = volData[6]
    for z in range(len(state)):
        if state[z] == "E":
            volData[6][z] = 1
        elif state[z] == "NE":
            volData[6][z] = 0
        else:
            val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
            location = horizon.index(val)
            volData[6][z] = location+1
    
    actives = [i for i, e in enumerate(volData[6]) if e != 0]
    hydroPlants_act = []; volData_act = [[] for x in range(columns-1)]
    for col in range(columns-1):
        for z in range(len(actives)):
            volData_act[col].append(volData[col][actives[z]])
    hPlantsReser = []
    for z in range(len(actives)):
        hydroPlants_act.append(hydroPlants[actives[z]])
        if (volData[2][actives[z]] > 0 and volData[3][actives[z]] > 0):
            hPlantsReser.append(1)
        else:
            hPlantsReser.append(0)
    
    # Identify areas by codde
    nameAreas = volData_act[11]
    for z in range(len(nameAreas)):
        location = areasData[0].index(nameAreas[z])
        volData_act[11][z] = location+1
    
    # Initial conditions of each reservoir
    for z in range(len(volData_act[0])):
        volIni = volData_act[0][z]*volData_act[2][z]
        volData_act[0][z] = volIni
        
    ###########################################################################
    
    substring = "HydroExpansion"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/HydroExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/HydroExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        expData = [[] for x in range(columns)]  
        for row in readCSV:
            expData[0].append(row[0])
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                expData[col+1].append(val)
    for col in range(columns):
        expData[col].pop(0)
    
    state = expData[4]
    for z in range(len(state)):
        val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
        location = horizon.index(val)
        expData[4][z] = location+1
    
    ###########################################################################
    
    substring = "Inflow"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/Inflow.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/Inflow.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        inflowData = [[] for x in range(columns)]  
        for row in readCSV:
            for col in range(2):
                val = row[col]
                try: 
                    val = int(row[col])
                except ValueError:
                    pass
                inflowData[col].append(val)
            for col in range(columns-2):
                val = row[col+2]
                try: 
                    val = float(row[col+2])
                except ValueError:
                    pass
                inflowData[col+2].append(val)
    
    inflowData_act = []
    inflowData_act.append(inflowData[0])
    inflowData_act.append(inflowData[1])
    for z in range(len(actives)):
        inflowData_act.append(inflowData[actives[z]+2])
        
    ###########################################################################
    
    substring = "WindConfiguration"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/WindConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/WindConfiguration.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        windPlants = []; windData = [[] for x in range(columns-1)]  
        for row in readCSV:
            tplants = row[0]; 
            windPlants.append(tplants)
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                windData[col].append(val)
        windPlants.pop(0)
    for col in range(columns-1):
        windData[col].pop(0)
    
    state = windData[6]
    for z in range(len(state)):
        if state[z] == "E":
            windData[6][z] = 1
        elif state[z] == "NE":
            windData[6][z] = 0
        else:
            val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
            location = horizon.index(val)
            windData[6][z] = location+1
    
    actives = [i for i, e in enumerate(windData[6]) if e != 0]
    windPlants_act = []; windData_act = [[] for x in range(columns-1)]
    for col in range(columns-1):
        for z in range(len(actives)):
            windData_act[col].append(windData[col][actives[z]])
    for z in range(len(actives)):
            windPlants_act.append(windPlants[actives[z]])
    
    # Identify areas by codde
    nameAreas = windData_act[8]
    for z in range(len(nameAreas)):
        location = areasData[0].index(nameAreas[z])
        windData_act[8][z] = location+1
    
    ###########################################################################
    
    substring = "WindExpansion"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/WindExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/WindExpansion.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        expWindData = [[] for x in range(columns)]  
        for row in readCSV:
            expWindData[0].append(row[0])
            for col in range(columns-1):
                val = row[col+1]
                try: 
                    val = int(row[col+1])
                except ValueError:
                    try:
                        val = float(row[col+1])
                    except ValueError:
                        pass
                expWindData[col+1].append(val)
    for col in range(columns):
        expWindData[col].pop(0)
    
    state = expWindData[1]
    for z in range(len(state)):
        val = datetime.datetime.strptime (state[z],"%Y-%m-%d %H:%M:%S") 
        location = horizon.index(val)
        expWindData[1][z] = location+1
        
    ###########################################################################   
    
    substring = "InflowWind"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/InflowWind.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/InflowWind.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        inflowWindData = [[] for x in range(columns)]  
        for row in readCSV:
            for col in range(2):
                val = row[col]
                try: 
                    val = int(row[col])
                except ValueError:
                    pass
                inflowWindData[col].append(val)
            for col in range(columns-2):
                val = row[col+2]
                try: 
                    val = float(row[col+2])
                except ValueError:
                    pass
                inflowWindData[col+2].append(val)
    
    inflowWindData_act = []
    inflowWindData_act.append(inflowData[0])
    inflowWindData_act.append(inflowData[1])
    for z in range(len(actives)):
        inflowWindData_act.append(inflowWindData[actives[z]+2])
            
    ###########################################################################
    
    substring = "Lines"
    xlxstocsv(tabnames,substring,importedfile)
    
    if numAreas is not 1:
        with open('datasystem/Lines.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV: columns = len(row); break
        with open('datasystem/Lines.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            linesData = [] 
            for row in readCSV:
                rowval=[]
                rowval.append(row[0]); rowval.append(row[1])
                for col in range(columns-2):
                    try:
                        val = float(row[col+2])
                    except ValueError:
                        pass    
                    rowval.append(val)
                linesData.append(rowval)
        linesData.pop(0)
        
        # Identify areas by code
        for z in range(len(linesData)):
            location1 = areasData[0].index(linesData[z][0])
            location2 = areasData[0].index(linesData[z][1])
            linesData[z][0] = location1+1; linesData[z][1] = location2+1
        
    else:
        with open('datasystem/Lines.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV: columns = len(row); break
        with open('datasystem/Lines.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            linesData = [] 
            for row in readCSV:
                rowval=[]
                for col in range(columns):
                    rowval.append(0)
                linesData.append(rowval)
        linesData.pop(0)    
    
    ###########################################################################
    
    substring = "LinesMods"
    xlxstocsv(tabnames,substring,importedfile)
                    
    with open('datasystem/LinesMods.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV: columns = len(row); break
    with open('datasystem/LinesMods.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        expLinesData = [[] for x in range(columns-2)]  

        for row in readCSV:
            
            for col in range(columns-2):
                
                val = row[col+2]
                try: 
                    val = int(val)
                except ValueError:
                    try:
                        val = float(val)
                    except ValueError:
                        pass
                
                expLinesData[col].append([row[0],row[1],val])
    
    for z in range(columns-2):
        val = datetime.datetime.strptime (expLinesData[z][0][2],"%Y-%m-%d %H:%M:%S") 
        location = horizon.index(val)
        expLinesData[z][0] = location+1
    
    # Identify areas by code
    for z in range(columns-2):
        for row in range(len(expLinesData[z])-1):
            location1 = areasData[0].index(expLinesData[z][row+1][0])
            location2 = areasData[0].index(expLinesData[z][row+1][1])
            expLinesData[z][row+1][0] = location1+1; expLinesData[z][row+1][1] = location2+1
    
    ###########################################################################
    
    # export data            
    DataDictionary = { "inflowData":inflowData_act,"inflowWindData":inflowWindData_act,
    "blocksData":blocksData,"horizon":horizon,"demandData":demandData,"rationingData":rationingData,
    "costData":costData_act,"thermalData":thermalData_act,"thermalPlants":thermalPlants_act,
    "hydroPlants":hydroPlants_act,"volData":volData_act,"windPlants":windPlants_act,
    "windData":windData_act,"battData":battData_act,"batteries":batteries_act,"linesData":linesData,
    "numAreas":numAreas,"areasData":areasData,"expData":expData,"expLines":expLinesData,
    "expThData":expThData,"hydroReservoir":hPlantsReser,"expWindData":expWindData,
    "expBttData":expBttData}
    
    pickle.dump(DataDictionary, open( "savedata/data_save.p", "wb" ) )
    
    # export data            
    DataDictionary2 = {"blocksData":blocksData,"rationingData":rationingData,
    "costData":costData_act,"thermalPlants":thermalPlants_act,
    "hydroPlants":hydroPlants_act,"volData":volData_act,"windPlants":windPlants_act,
    "battData":battData_act,"batteries":batteries_act,"linesData":linesData,
    "numAreas":numAreas,"hydroReservoir":hPlantsReser}
    
    pickle.dump(DataDictionary2, open( "savedata/data_save_iter.p", "wb" ) )
   
    
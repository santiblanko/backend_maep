def inputwind(dict_data,dict_format,stages):
    
    import pickle
    
    windPlants = dict_data['windPlants']
    windData = dict_data['windData']
    scenarios = dict_format['scenarios']
    yearvector = dict_format['yearvector']
    inflowWindData = dict_data['inflowWindData']
    numAreas = dict_data['numAreas']
    expWindData = dict_data['expWindData']
    
    speed_wind = [[[] for _ in range(2) ] for _ in range(len(windPlants))]
    
    # Production energy limits
    numFarms = len(windPlants) # Wind plants
    prodFactor = [x for x in windData[1]] # Production losses of wind plants
    w_hat = [] # Limits in the volume turbined at each stage by each hydro plant
    for i in range(numFarms):
        w_hat1 = [x * windData[0][i] * (1-(windData[10][i]/100)) for x in yearvector[0:]]
        for j in range(len(w_hat1)):
            if windData[6][i] > j+1:
                w_hat1[j] = 0
        w_hat.append(w_hat1)
        
    # Expansion of capacity of wind plants
    if len(expWindData[0]) > 0:
        
        for i in range(len(expWindData[0])): # loop in modificated plants
            index = windPlants.index(expWindData[0][i])
            stagemod = expWindData[1][i]
            gmaxplant = [x * expWindData[2][i] * (1-(expWindData[6][i]/100)) for x in yearvector[stagemod-1:]]
            
            for z in range(stagemod,stages+1):
                w_hat[index][z-1] = gmaxplant[z-stagemod]
                
    hat_area = [[0]*len(w_hat1) for _ in range(numAreas) ] 
    for i in range(numFarms):
        areafarm = windData[8][i]
        hat_area[areafarm-1] = [sum(x) for x in zip(hat_area[areafarm-1], w_hat[i])]
    
    # Wind speed
    import numpy as np
    for i in range(stages):
        # Series Wind
        for k in range(2,2+len(windPlants)):
            
            stage_wind = inflowWindData[k][scenarios*i+1:scenarios*(i+1)+1]
            if windData[6][k-2] > i+1: # initial stage for inflows
                stage_wind[:] = [x*0 for x in stage_wind]
            aux_wind = np.hstack((speed_wind[k-2][1], stage_wind))        
            speed_wind[k-2][1] = aux_wind
            
    # wind inflow series
    for i in range(len(windPlants)):
        aux_resize_s = np.resize(speed_wind[i][1], [stages,scenarios]) 
        speed_wind[i][0] = inflowWindData[2+i][0] 
        speed_wind[i][1] = aux_resize_s  
    
  
    # export data
    DataDictionary = {"w_limit":w_hat, "losses_wind":prodFactor, "speed_wind":speed_wind,
                      "hat_area":hat_area}
    pickle.dump(DataDictionary, open( "savedata/wind_save.p", "wb" ) )
    
    return DataDictionary

def inputInflowWind(dict_data,dict_format,dict_wind,stages,deviation,speed_out,
    speed_in,eps_area,eps_all):
    
    import pickle
    from utils.parameters import inflowswind
    
    windPlants = dict_data['windPlants']
    blocksData = dict_data['blocksData']
    scenarios = dict_format['scenarios']
    speed_wind = dict_wind['speed_wind']
    windData = dict_data['windData']
    numAreas = dict_data['numAreas']

    #import numpy as np
    from scipy.stats import norm
    #from statistics import stdev
    
    speed_area=[]; speed_quantile=[]
    
    for area in range(numAreas):
        
        # Set of plants for each area
        windP = 0; speed_wind_temp = []; intensity=[]
        for i in range(len(windPlants)):
            if windData[8][i] == area+1:
                windP += 1
                speed_wind_temp.append(speed_wind[i])
                intensity.append(blocksData[i+3])
        
        if windP is not 0:
            
            # Inflow by blocks with short term variability
            speed_wind_var = [[[[] for x in range(scenarios) ] for y in range(stages)] for z in range(windP)]
            for i in range(windP):
                for n in range(stages): 
                    scenario_inflow = speed_wind_temp[i][1][n]
                    for k in range(scenarios): 
                        speed_sc = []
                        for j in range(len(blocksData[0])): 
                            mu = scenario_inflow[k]*intensity[i][j]
                            var = deviation * mu
                            speed_sc.append([mu,var])
                        speed_wind_var[i][n][k]= speed_sc 
            
            # Quantile F_out area
            speed_fout = [[[] for x in range(scenarios) ] for y in range(stages)]
            for n in range(stages): 
                for k in range(scenarios): 
                    for j in range(len(blocksData[0])): 
                        speed_sc = [0,0]
                        for i in range(windP):
                            speed_sc = [speed_sc[x]+speed_wind_var[i][n][k][j][x] for x in range(2)] 
                        
                        if speed_sc[0] == 0:
                            cdf_out = 0 # Wind plants with later intalation date
                        else:
                            #norm_speed_in = (speed_in-(speed_sc[0]/windP))/(speed_sc[1]**0.5/windP)
                            norm_speed_out = (speed_out-(speed_sc[0]/windP))/(speed_sc[1]**0.5/windP)
                            #cdf_in = norm.cdf(norm_speed_in)
                            cdf_out = norm.cdf(norm_speed_out)
                        
                        if (cdf_out+eps_area-1) < 0:
                            speed_fout[n][k].append(9999) # No operation
                        else:
                            speed_fout[n][k].append(norm.ppf(cdf_out+eps_area-1)) # Quantile
        else:
            # Inflow by blocks with short term variability
            speed_wind_var = [[[[[0,0] for j in range(len(blocksData[0]))] for x in range(scenarios) ] for y in range(stages)] for z in range(1)]
            
            # Quantile F_out area
            speed_fout = [[ [9999] * (len(blocksData[0])) for x in range(scenarios) ] for y in range(stages)]

        # Save areas
        speed_area.append(speed_wind_var)
        speed_quantile.append(speed_fout)
        

    # General quantile
    wind_var_all = [[[[] for x in range(scenarios) ] for y in range(stages)] for z in range(len(windPlants))]
    for i in range(len(windPlants)):
        for n in range(stages): 
            scenario_inflow = speed_wind[i][1][n]
            for k in range(scenarios): 
                speed_sc = []
                for j in range(len(blocksData[0])): 
                    mu = scenario_inflow[k]*blocksData[i+3][j]
                    var = deviation * mu
                    speed_sc.append([mu,var])
                wind_var_all[i][n][k]= speed_sc 
                    
                    
    speed_fout_all = [[[] for x in range(scenarios) ] for y in range(stages)]
    for n in range(stages): 
        for k in range(scenarios): 
            for j in range(len(blocksData[0])): 
                speed_sc = [0,0]
                for i in range(len(windPlants)):
                    speed_sc = [speed_sc[x]+wind_var_all[i][n][k][j][x] for x in range(2)] 
                
                if speed_sc[0] == 0:
                    cdf_out = 0 # Wind plants with later intalation date
                else:
                #norm_speed_in = (speed_in-(speed_sc[0]/windP))/(speed_sc[1]**0.5/windP)
                    norm_speed_out = (speed_out-(speed_sc[0]/len(windPlants)))/(speed_sc[1]**0.5/len(windPlants))
                    #cdf_in = norm.cdf(norm_speed_in)
                    cdf_out = norm.cdf(norm_speed_out)
                
                if (cdf_out+eps_all-1) < 0:
                    speed_fout_all[n][k].append(9999)
                else:
                    speed_fout_all[n][k].append(norm.ppf(cdf_out+eps_all-1))
    
    # Energy calculation
    windenergy = inflowswind(stages,scenarios,speed_quantile,speed_area,dict_data,dict_format)
    
    # export data: return mean and variance, as well al the inverse of normal standard
    # DataDictionary = {"wind_var":speed_area, "wind_quantile":speed_quantile, "windenergy":windenergy,
    #                  "wind_quantile_all":speed_fout_all, "wind_var_all":wind_var_all}
    
    DataDictionary = {"windenergy":windenergy}
    pickle.dump(DataDictionary, open( "savedata/windspeed_save.p", "wb" ) )
    
    # return DataDictionary
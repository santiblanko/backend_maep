def inputbatteries(dict_data,dict_format,stages):
    
    import pickle
    
    yearvec = dict_format['yearvector']
    batteries = dict_data['batteries']
    battData = dict_data['battData']
    expBttData = dict_data['expBttData']
    
    numBatteries = len(batteries) # batteries
    b_hat = []; b_storage_hat = [] # Limits in the energy delivered at each stage by each batterie
    for i in range(numBatteries):
        b_hat1 = [x*battData[3][i] for x in yearvec[0:]]
        b_hat2 = [x*(battData[5][i]*3600) for x in yearvec[0:]]
        b_hataux = []; b_storage_aux = []    
        for j in range(len(b_hat1)):
            aux = min(b_hat1[j],b_hat2[j]) 
            aux2 = b_hat1[j] * battData[2][i] * (1-(battData[11][i]/100)) # max storage
            aux3 = b_hat1[j] * battData[1][i] # min storage
            if battData[6][i] > j+1:
                aux = 0; aux2 = 0; aux3 = 0
            b_hataux.append(aux)
            b_storage_aux.append([aux2,aux3])
        b_hat.append(b_hataux); b_storage_hat.append(b_storage_aux)
    
    b_area = []
    for n in range(len(batteries)): 
        b_area.append(battData[9][n])
    
    # Expansion of batteries capacity
    if len(expBttData[0]) > 0:
        
        for i in range(len(expBttData[0])): # loop in modificated plants
            index = batteries.index(expBttData[0][i])
            stagemod = expBttData[1][i]
            gmaxplant = [x * expBttData[2][i] * (1-(expBttData[6][i]/100)) for x in yearvec[stagemod-1:]]
            
            for z in range(stagemod,stages+1):
                b_storage_hat[index][z-1][0] = gmaxplant[z-stagemod]
                
    # export data
    DataDictionary = {"b_limit":b_hat,"b_area":b_area, "b_storage":b_storage_hat}
    
    pickle.dump(DataDictionary, open( "savedata/batt_save.p", "wb" ) )
    
    # return DataDictionary

def inputlines(dict_data,dict_format,stages):

    import pickle
    
    yearvector = dict_format['yearvector']
    numAreas = dict_data['numAreas']
    linesData = dict_data['linesData']
    expLines = dict_data['expLines']
    
    # Lines limits
    limitStages = []
    for stage in range(stages):
    
        LinesMatrix = [[0 for x in range(numAreas)] for y in range(numAreas)] 
        for z in range(len(linesData)):
            LinesMatrix[int(linesData[z][0])-1][int(linesData[z][1])-1]=linesData[z][2]*yearvector[stage]
        
        limitStages.append(LinesMatrix)
    
    if len(expLines) > 0:
        
        for i in range(len(expLines)):
            
            if expLines[i][0] <= stages:
                
                for stage in range(expLines[i][0],stages):
                    
                    LinesMatrix = [[0 for x in range(numAreas)] for y in range(numAreas)] 
                    for z in range(len(expLines[i])-1):
                        LinesMatrix[int(expLines[i][z+1][0])-1][int(expLines[i][z+1][1])-1]=expLines[i][z+1][2]*yearvector[stage-1]
                    
                    limitStages[stage-1]= LinesMatrix
            
    # export data
    DataDictionary = {"l_limits":limitStages}
    
    pickle.dump(DataDictionary, open( "savedata/lines_save.p", "wb" ) )
    
    # return limitStages
